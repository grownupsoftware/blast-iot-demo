import {Component, OnInit, Input, Output, EventEmitter, ChangeDetectionStrategy} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {MdSlider, MdSlideToggle, MdSlideToggleChange} from "@angular/material";
import {trigger, state, style, transition, animate} from '@angular/animations';


@Component({
    moduleId: module.id,
    selector: 'plug-control',
    template: `
    `
})
export class PlugControlComponent {

}

@Component({
    moduleId: module.id,
    selector: 'plug',
    template: `
    <div [ngStyle]="{width: width+'px',height: height+'px'}">

<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64" style="enable-background:new 0 0 64 64">
  <path d="m4.5 4.5h55v55h-55z" style="fill:#f5f5f5"/>
  <path d="m59 5v54h-54v-54h54m1-1h-1-54-1v1 54 1h1 54 1v-1-54-1z" style="fill:#c8c8c8"/>
  <path [@status]="status" d="m13.5 42.5h10v6h-10z" style="fill:#212121"/>
  <path d="m23 43v5h-9v-5h9m1-1h-1-9-1v1 5 1h1 9 1v-1-5-1z" style="fill:#dcdcdc"/>
  <path [@status]="status" d="m40.5 42.5h10v6h-10z" style="fill:#212121"/>
  <path d="m50 43v5h-9v-5h9m1-1h-1-9-1v1 5 1h1 9 1v-1-5-1z" style="fill:#dcdcdc"/>
  <path [@status]="status" d="m29.5 12.5h5v11h-5z" style="fill:#212121"/>
  <path  d="m34 13v10h-4v-10h4m1-1h-1-4-1v1 10 1h1 4 1v-1-10-1z" style="fill:#dcdcdc"/>
</svg>

    </div>
     `,
    styles: [],

    animations: [
        //        trigger('switchOn'        , [
        //            state('1', style({display: 'show'}        )),
        //            state('0', style({display: 'none'        }))
        //                ]),
        //        trigger('switchOff'        , [
        //            state('1', style({display: 'none'}        )),
        //            state('0', style({display: 'show'        }))
        //        ])

        trigger('status', [
            state('1', style({fill: 'green'})),
            state('0', style({fill: 'red'})),
            transition('off => on', animate('300ms')),
            transition('on => off', animate('300ms'))
        ])
    ]
})
export class PlugComponent {
    @Input() status: boolean;
    @Input() width: number;
    @Input() height: number;
}

@Component({
    moduleId: module.id,
    selector: 'single-plug',
    template: `
    <div class="single-plug">
        <div class="switch">
            <switch [status]="status" (change)="onSwitchChange($event)" [width]="switchWidth" [height]="switchHeight"></switch>
        </div>    
        <div class="plug">
            <plug [status]="status"  [width]="plugWidth" [height]="plugHeight"></plug>
        </div>    
    </div>    
     `,
    styles: [
        '.switch {display: inline-block;}',
        '.plug {display: inline-block;}',
        '.single-plug {border: solid 1px #888888; border-radius:4px;box-shadow: 2px 5px 2px #888888;width:154px}'
    ],
})
export class SinglePlugComponent {
    @Input() status: boolean;
    @Input() width: number;
    @Input() height: number;

    // if changing these change the css above
    plugWidth: number = 100;
    plugHeight: number = 100;
    switchWidth: number = 50;
    switchHeight: number = 50;

    onSwitchChange(event) {
        this.status = !this.status;
    }

}

@Component({
    moduleId: module.id,
    selector: 'double-plug',
    template: `
    <div class="double-plug">
        <div class="plug">
            <plug [status]="left"  [width]="plugWidth" [height]="plugHeight"></plug>
        </div>    
        <div class="switch">
            <switch [status]="left" (change)="onLeft($event)" [width]="switchWidth" [height]="switchHeight"></switch>
        </div>    
        <div class="switch">
            <switch [status]="right" (change)="onRight($event)" [width]="switchWidth" [height]="switchHeight"></switch>
        </div>    
        <div class="plug">
            <plug [status]="right"  [width]="plugWidth" [height]="plugHeight"></plug>
        </div>    
    </div>    
     `,
    styles: [
        '.switch {display: inline-block;}',
        '.plug {display: inline-block;}',
        '.double-plug {border: solid 1px #888888; border-radius:4px;box-shadow: 2px 5px 2px #888888;width:152px}'
    ],
})
export class DoublePlugComponent {
    @Input() left: boolean;
    @Input() right: boolean;
    @Input() width: number;
    @Input() height: number;
    plugWidth: number = 50;
    plugHeight: number = 50;
    switchWidth: number = 20;
    switchHeight: number = 20;

    onLeft(event) {
        this.left = !this.left;
    }

    onRight(event) {
        this.right = !this.right;
    }

}

export const COMPONENTS = [
    PlugControlComponent,
    PlugComponent,
    SinglePlugComponent,
    DoublePlugComponent
]