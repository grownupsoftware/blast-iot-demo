import {Component, Input, Output, EventEmitter} from '@angular/core';
import {trigger, state, style} from '@angular/animations';

@Component({
    moduleId: module.id,
    selector: 'switch',
    templateUrl: './switch.component.html',
    styles: [],
    animations: [
        // state uses status (boolean) and '1' = true and '0' = false
        trigger('switchOn', [
            state('1', style({display: 'show'})),
            state('0', style({display: 'none'}))
        ]),
        trigger('switchOff', [
            state('1', style({display: 'none'})),
            state('0', style({display: 'show'}))
        ])
    ]
})
export class SwitchComponent {
    @Input() status: boolean;
    @Output() change = new EventEmitter();
    @Input() width: number;
    @Input() height: number;
//    
//    onSwitchClick() {
//        this.change.emit(this.status);
//    }
}