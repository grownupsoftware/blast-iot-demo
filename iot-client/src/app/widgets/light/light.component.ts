import {Component, OnInit, Input, Output, EventEmitter, ChangeDetectionStrategy} from '@angular/core';
import {MdSlideToggleChange, MdSliderChange} from "@angular/material";
import {trigger, state, style, transition, animate} from '@angular/animations';


@Component({
    moduleId: module.id,
    selector: 'light',
    template: '<light-detail [status]="status" [dimmable]="dimmable" ></light-detail>',
    styles: []
})
export class LightComponent implements OnInit {
    data: any = {};
    dimmable: boolean = false;
    on: boolean = true;
    off: boolean = true;
    status: boolean = this.on;
    value: number = 0;

    constructor() {
    }

    setData(data: any) {
        this.data = data;
    }

    ngOnInit() {
    }
}

@Component({
    moduleId: module.id,
    selector: 'light-detail',
    template: `
        <ticker-clocks></ticker-clocks>
    <!--
    <div style="padding:20px;width:200px;height:200px">
        <clock title='IOT Time' [useTicker]="true"></clock>
        <clock title="Local Time" [useTicker]="false"></clock>
    </div>    
    -->
    <div class="switch">
        <switch [status]="status" (change)="onSwitchChange($event)" [width]="switchWidth" [height]="switchHeight"></switch>
    </div>
    <div  class="light" >
        <bulb [status]="status" [width]="bulbWidth" [height]="bulbHeight"></bulb>
    </div>    
    <div style="padding:20px;">
        <single-plug [status]="status"  [width]="bulbWidth" [height]="bulbHeight"></single-plug>
    </div>    
    <div style="padding:20px;">
        <double-plug [left]="left" [right]="right"  [width]="bulbWidth" [height]="bulbHeight"></double-plug>
    </div>    
    <div style="padding:20px;">
        <kettle></kettle>
    </div>    
    `,
    styleUrls: ['./light-detail.component.css']
    //    animations: [
    //        trigger('visibilityChanged', [
    //            state('1', style({'fill-opacity': 1, fill: '#ffc84d'})),
    //            state('0', style({'fill-opacity': 0.1, fill: '#eee'})),
    //            transition('off => on', animate('1300ms')),
    //            transition('on => off', animate('1300ms')),
    //            //transition('* => *', animate('.5s'))
    //        ])
    //        //        trigger('switchOn', [
    //        //            state('on', style({display: 'show'})),
    //        //            state('off', style({display: 'none'}))
    //        //            //            transition('off => on', animate('600ms')),
    //        //            //            transition('on => off', animate('600ms')),
    //        //            //transition('* => *', animate('.5s'))
    //        //        ]),
    //        //        trigger('switchOff', [
    //        //            state('on', style({display: 'none'})),
    //        //            state('off', style({display: 'show'}))
    //        //            //            transition('off => on', animate('600ms')),
    //        //            //            transition('on => off', animate('600ms')),
    //        //            //transition('* => *', animate('.5s'))
    //        //        ])
    //    ]
})
export class LightDetailComponent {
    @Input() status: boolean;
    @Input() dimmable: boolean;
    bulbWidth: number = 100;
    bulbHeight: number = 100;
    switchWidth: number = 50;
    switchHeight: number = 50;
    level: string = '60%';

    left: boolean = true;
    right: boolean = false;


    onDimmableChange(event: any) {
        console.log("This is emitted as the thumb slides", event);
    }

    onSwitch(event: MdSlideToggleChange) {
        this.status = event.checked;
    }

    onSwitchChange(event) {
        this.status = !this.status;
    }

}

export const COMPONENTS = [
    LightComponent,
    LightDetailComponent,
];
