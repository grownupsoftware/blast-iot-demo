import {Component, OnInit, Input, Output, EventEmitter, ChangeDetectionStrategy, Inject, ElementRef, ViewChild, ChangeDetectorRef} from '@angular/core';
import {MdSlider, MdSlideToggle, MdSlideToggleChange, MdSliderChange} from "@angular/material";
import {trigger, state, style, transition, animate} from '@angular/animations';
import {Observable} from 'rxjs/Observable';

import "gsap";
//const TweenMax = require('gsap').TweenMax;
//const Cubic = require('gsap').Cubic;
declare var TimelineMax, ease, TimelineMax, TweenMax, Cubic, Power4, Power1, Power2, Power3, Bounce, Elastic: any;

@Component({
    moduleId: module.id,
    selector: 'kettle',
    changeDetection: ChangeDetectionStrategy.OnPush,
    template: `
    <div class="switch">
        <switch [status]="status" (change)="onSwitchChange($event)" [width]="switchWidth" [height]="switchHeight"></switch>
    </div>
    <div class="kettle-pot">
        <kettle-pot [status]="status" [width]="kettleWidth" [height]="kettleHeight" [level]="level" [boiling]="boiling" ></kettle-pot>
    </div>    
    <div *ngIf="debug === true" >
        <div>
            <md-slider min="0" max="100" (input)="onLevelChange($event)"></md-slider>
        </div>
        <div>
            <md-slide-toggle class="example-margin" ngDefaultControl (change)="onBoil($event)" [checked]="boiling">Boil?</md-slide-toggle>
        </div>
    </div>
    `
})
export class KettleComponent {
    data: any = {};
    status: boolean = false;
    level: string = '60%';
    boiling: boolean = false;
    debug: boolean = true;

    kettleWidth: number = 100;
    kettleHeight: number = 100;
    switchWidth: number = 50;
    switchHeight: number = 50;

    constructor(private cd: ChangeDetectorRef) {

    }
    setData(data: any) {
        this.data = data;
    }

    onSwitchChange(event) {
        this.status = !this.status;
        if (this.status === false) {
            this.boiling = false;
        }
    }

    onLevelChange(event: MdSliderChange) {
        this.level = event.value + '%';
        this.cd.markForCheck();
    }

    onBoil() {
        this.boiling = !this.boiling;
        this.cd.markForCheck();
        //this.boiling === true ? this.startBoiling() : this.stopBoiling();
    }

}

@Component({
    moduleId: module.id,
    selector: 'kettle-pot',
    changeDetection: ChangeDetectionStrategy.OnPush,
    template: `
    <div [ngStyle]="{width: width+'px',height: height+'px'}">
        <svg
           xmlns:svg="http://www.w3.org/2000/svg"
           xmlns="http://www.w3.org/2000/svg"
           xmlns:xlink="http://www.w3.org/1999/xlink"
           width="100%"
           height="100%"
           viewBox="0 0 512 512"
           style="enable-background:new 0 0 512 512;"
           x="0px"
           y="0px" >
           <svg:defs>
              <svg:linearGradient  x1="0%" y1="100%" x2="0%" y2="0%" id="water">
                <svg:stop   [attr.offset]="level" style="stop-color:blue"/>
                <svg:stop offset="0%" style="stop-color:white"/>
              </svg:linearGradient>
           </svg:defs>
           
          <svg:path [@visibilityChanged]="status"
             id="path5689"
             d="m 381.207,159.189 -6.272,-17.067 c -1.613,-4.42 -6.528,-6.699 -10.948,-5.06 -4.42,1.613 -6.69,6.519 -5.06,10.948 l 6.263,17.067 c 1.263,3.456 4.531,5.589 8.004,5.589 0.981,0 1.98,-0.171 2.944,-0.529 4.429,-1.612 6.69,-6.519 5.069,-10.948 z"
             />
          <svg:path  [@visibilityChanged]="status"
             id="path5691"
             d="m 208.72753,287.47271 c 0,111.27338 20.37893,167.69525 60.56016,167.69525 40.18123,0 60.56016,-56.42187 60.56016,-167.69525 0,-111.27339 -20.37893,-167.69526 -60.56016,-167.69526 -40.18123,0 -60.56016,56.42187 -60.56016,167.69526 z m 108.45924,0 c 0.49309,139.37165 -37.87232,153.98387 -49.57699,153.98387 -11.70289,0 -45.43788,-14.59813 -42.86534,-152.30596 3.02761,-162.06732 33.67754,-156.50075 45.38221,-156.50075 11.70467,0 46.62029,30.50722 47.06012,154.82284 z"
             style="fill:#000000;fill-opacity:1;stroke:none;stroke-opacity:1" />
          <svg:path [@visibilityChanged]="status"
             id="path5693"
             d="m 418.38215,177.92604 c 0,0 1.74238,1.58688 3.23538,-2.88512 L 477.42,96.563 c 0.879,-2.603 0.435,-5.461 -1.161,-7.68 -1.613,-2.236 -4.19,-3.55 -6.929,-3.55 l -80.12,0 C 360.385,28.757 324.212,0 281.597,0 234.041,0 193.917,28.757 162.224,85.333 l -64.094,0 c -3.388,0 -6.46,2.005 -7.817,5.112 -34.158,78.088 -49.723,162.986 -55.961,206.652 -1.792,12.518 7.731,24.602 21.231,26.923 11.964,2.065 25.788,-4.565 29.713,-24.03 1.536,-7.603 3.063,-15.761 4.685,-24.431 7.202,-38.417 16.094,-85.905 35.43,-139.034 l 9.916,-0.008 c 1.109,0 2.176,-0.009 3.183,-0.009 C 99.376,238.839 85.33,378.377 85.33,469.333 l 0,34.133 c 0,4.71 3.823,8.533 8.533,8.533 l 358.4,0 c 4.719,0 8.533,-3.823 8.533,-8.533 l 0,-34.133 c 0,-96.64 -14.754,-245.769 -56.363,-348.015 -1.766,-4.369 -6.767,-6.468 -11.119,-4.693 -4.369,1.775 -6.468,6.758 -4.685,11.128 40.67,99.968 55.1,246.485 55.1,341.581 l 0,25.6 -341.332,0 0,-17.067 315.733,0 c 4.719,0 8.533,-3.823 8.533,-8.533 0,-4.71 -3.814,-8.533 -8.533,-8.533 l -315.682,0 c 0.853,-76.919 13.278,-223.01 55.39,-326.784 1.408,-1.28 1.843,-3.226 1.843,-6.178 0,-4.07 -2.876,-7.569 -6.869,-8.371 -0.845,-0.162 -1.681,-0.205 -2.5,-0.119 -2.722,0.085 -20.164,0.119 -30.848,0.119 -3.558,0 -6.741,2.21 -7.996,5.538 -21.274,56.745 -30.694,107.015 -38.255,147.404 -1.613,8.593 -3.132,16.674 -4.651,24.209 -0.896,4.446 -3.405,11.742 -10.086,10.581 -3.84,-0.657 -7.748,-4.087 -7.236,-7.689 5.982,-41.847 20.685,-122.3 52.506,-197.111 l 353.741,0 -35.26832,65.57 c -1.495,4.472 -8.30853,8.46204 -3.83653,9.95604 z M 181.962,85.333 c 27.563,-45.269 61.022,-68.267 99.635,-68.267 34.321,0 63.309,22.4 88.354,68.267 z"/>
          <svg:path [@visibilityChanged]="status"
             id="path5695"
             d="m 396.831,299.435 c 0.401,4.429 4.122,7.765 8.491,7.765 0.256,0 0.521,-0.009 0.776,-0.034 4.693,-0.427 8.158,-4.574 7.731,-9.267 -4.659,-51.567 -21.026,-102.153 -21.726,-104.277 -1.459,-4.48 -6.238,-6.929 -10.76,-5.47 -4.471,1.468 -6.921,6.281 -5.453,10.76 0.154,0.503 16.461,50.859 20.941,100.523 z"/>
          <svg:path class="kettle-color" 
             style="stroke:none;stroke-width:10;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1"
             d="m 103.14195,486.6294 0,-7.97009 157.09479,-0.005 c 107.67667,-0.003 158.09819,-0.28331 160.28451,-0.89043 5.45653,-1.51524 8.27235,-8.72595 5.14892,-13.18528 -3.26148,-4.65606 7.25148,-4.37632 -164.47711,-4.37632 l -158.31918,0 0.54069,-19.50576 c 2.51885,-90.86965 17.9216,-192.66717 40.00878,-264.42 4.95073,-16.08308 12.7923,-38.01702 15.37865,-43.01624 1.72796,-3.34001 1.76445,-6.32045 0.11642,-9.5074 -2.35198,-4.54822 -4.46028,-5.00628 -23.04241,-5.00628 -18.15737,0 -21.38583,0.58809 -23.79503,4.33445 -0.69243,1.07675 -3.75552,9.30628 -6.80686,18.28784 -13.215193,38.89863 -20.068572,66.41522 -31.951429,128.28621 -3.046454,15.86211 -5.914224,29.83075 -6.372821,31.04143 -2.095339,5.5316 -6.532699,7.35324 -11.568488,4.74914 -4.304266,-2.22583 -4.321246,-3.09055 -0.512703,-26.1099 9.584652,-57.93097 25.035727,-115.77686 43.165492,-161.6034 l 5.739009,-14.50648 176.37545,-0.2117 c 141.27073,-0.16957 176.29496,-0.002 175.97109,0.84199 -0.22239,0.57953 -8.0025,15.14829 -17.28914,32.37503 -9.28664,17.22674 -16.89866,31.76031 -16.9156,32.29683 -0.0169,0.53652 -2.61597,-6.7639 -5.77562,-16.22314 -7.04255,-21.08376 -11.22935,-31.57185 -13.61857,-34.11506 -3.871,-4.12049 -11.02335,-2.96814 -13.69962,2.20721 -1.84449,3.56685 -1.66121,4.7288 2.40632,15.25557 27.09347,70.11781 45.86253,174.86632 50.78752,283.44075 0.45001,9.92067 1.05867,30.96801 1.35259,46.77188 l 0.53438,28.73429 -170.38001,0 -170.38002,0 0,-7.9701 z m 182.27401,-34.65605 c 21.80192,-10.73305 35.19105,-41.29888 41.23025,-94.12392 5.22644,-45.71582 4.51642,-112.69102 -1.62948,-153.70629 -6.54148,-43.65511 -19.84816,-71.50849 -38.53348,-80.6578 -6.36009,-3.11422 -12.27159,-4.18383 -20.34086,-3.68041 -30.80718,1.92199 -48.87674,37.81739 -55.63671,110.52297 -2.48896,26.7696 -2.49207,87.08639 -0.006,114.09822 5.29771,57.5584 17.73049,92.00279 37.87436,104.92907 6.9621,4.46757 12.5141,5.84279 22.59166,5.59592 8.07348,-0.19779 9.32858,-0.45643 14.45014,-2.97776 z M 411.23584,305.05791 c 2.64197,-2.3664 2.73412,-2.66517 2.57953,-8.36341 -0.4749,-17.50543 -8.29956,-57.7327 -17.64148,-90.69636 -4.53221,-15.99224 -6.11501,-18.45707 -11.85229,-18.45707 -4.02567,0 -8.131,3.03761 -8.91217,6.59428 -0.34502,1.57084 0.85773,7.21609 3.61473,16.96621 7.57282,26.78129 13.00259,52.75379 16.1214,77.11441 0.91568,7.15211 1.87982,13.69057 2.14256,14.5299 0.67893,2.16886 5.2988,4.75929 8.49449,4.76298 1.81874,0.003 3.61676,-0.80601 5.45323,-2.45094 z m -31.91148,-136.8492 c 2.34169,-2.34168 2.51963,-2.9151 2.14778,-6.9214 -0.22299,-2.4025 -1.99754,-8.4326 -3.94343,-13.40021 -2.86631,-7.31728 -3.99515,-9.26843 -5.94596,-10.27723 -6.34567,-3.28147 -13.39773,0.63649 -13.39773,7.44348 0,4.09521 6.71232,20.96943 9.30876,23.40144 3.5493,3.32453 8.36003,3.22447 11.83058,-0.24608 z"
             id="path6269"/>
          <svg:path  class="kettle-color"
             style="stroke:none;stroke-width:10;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1"
             d="m 184.70551,81.788983 c 3.9301,-6.984613 17.4829,-24.130429 25.03475,-31.671776 16.88603,-16.862537 34.11026,-26.768522 53.64257,-30.850866 10.63996,-2.223804 26.86625,-2.055161 36.0191,0.374354 19.89613,5.281194 37.72258,18.555423 53.48154,39.824352 5.71683,7.715659 16.20799,24.092332 16.20799,25.300623 0,0.232426 -41.9172,0.422594 -93.14932,0.422594 l -93.14933,0 1.9127,-3.399281 z"
             id="path6271"/>
          <svg:path 
            style="fill:url(#water);stroke:none"
             d="m 257.24966,442.04449 c -27.4025,-12.5729 -41.27488,-74.861 -37.49875,-168.37217 2.66187,-65.9171 9.35148,-105.11263 21.69064,-127.08902 8.61392,-15.34164 22.73445,-21.01758 35.18246,-14.1421 16.27393,8.98869 30.85643,39.15454 38.122,78.86041 8.215,44.89435 8.25045,113.57279 0.0818,158.38576 -5.40826,29.66938 -14.59683,51.32431 -27.02445,63.68912 -10.46227,10.40942 -20.532,13.26618 -30.55366,8.668 l 10e-6,0 z"
             id="path6275"/>             
          <svg:path  [@boilingChanged]="boiling"
            style="fill:none;fill-rule:evenodd;stroke:#917a7a;stroke-width:10;stroke-linecap:butt;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1"
            d="m 426.61083,78.484132 c 19.34282,-21.496686 -20.65792,-44.65961 4.96394,-68.2913"
            id="boil1"/>
          <svg:path  [@boilingChanged]="boiling"
            style="fill:none;fill-rule:evenodd;stroke:#917a7a;stroke-width:10;stroke-linecap:butt;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1"
            d="M 455.78273,78.357897 C 476.46682,58.148486 438.03705,32.463217 465.1244,10.526668"
            id="boil2"/>
          <svg:path  [@boilingChanged]="boiling"
            style="fill:#ffffff;fill-rule:evenodd;stroke:#ffffff;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1;fill-opacity:1"
            d="m 261.76759,370.31721 c -2.97821,0.72968 -5.26881,3.28787 -5.981,6.22692 -1.13788,4.39937 0.28246,9.60195 4.03139,12.33894 1.55378,1.03775 3.3748,1.71661 5.23408,1.92242 5.70066,0.5281 11.46013,-4.19348 11.70151,-9.98532 0.0566,-2.10618 -0.60938,-4.19959 -1.64228,-6.0187 -2.28688,-3.85555 -7.10882,-6.29284 -11.55156,-5.15183 -0.61885,0.15802 -1.22049,0.38278 -1.79214,0.66757 z"
            id="bubble1" />
          <svg:path  [@boilingChanged]="boiling"
            style="fill:#ffffff;fill-rule:evenodd;stroke:#ffffff;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1;fill-opacity:1"
            d="m 245.67564,408.24824 c -2.97821,0.72968 -5.26881,3.28787 -5.981,6.22692 -1.13788,4.39937 0.28246,9.60195 4.03139,12.33894 1.55378,1.03775 3.3748,1.71661 5.23408,1.92242 5.70066,0.5281 11.46013,-4.19348 11.70151,-9.98532 0.0566,-2.10618 -0.60938,-4.19959 -1.64228,-6.0187 -2.28688,-3.85555 -7.10882,-6.29284 -11.55156,-5.15183 -0.61885,0.15802 -1.22049,0.38278 -1.79214,0.66757 z"
            id="bubble2" />
          <svg:path  [@boilingChanged]="boiling"
            style="fill:#ffffff;fill-rule:evenodd;stroke:#ffffff;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1;fill-opacity:1"
            d="m 258.86245,421.43505 c -2.97821,0.72968 -5.26881,3.28787 -5.981,6.22692 -1.13788,4.39937 0.28246,9.60195 4.03139,12.33894 1.55378,1.03775 3.3748,1.71661 5.23408,1.92242 5.70066,0.5281 11.46013,-4.19348 11.70151,-9.98532 0.0566,-2.10618 -0.60938,-4.19959 -1.64228,-6.0187 -2.28688,-3.85555 -7.10882,-6.29284 -11.55156,-5.15183 -0.61885,0.15802 -1.22049,0.38278 -1.79214,0.66757 z"            
            id="bubble3" />

        </svg>
    </div>
     `,
    styles: ['.kettle-color {fill:#4acbc1}'],
    animations: [
        trigger('visibilityChanged', [
            state('1', style({'fill-opacity': 1, fill: 'green'})),
            state('0', style({'fill-opacity': 1, fill: '#535051'})),
            transition('1 => 0', animate('300ms')),
            transition('0 => 1', animate('300ms')),
            //transition('* => *', animate('.5s'))
        ]),

        trigger('boilingChanged', [
            state('0', style({'fill-opacity': 0, 'stroke-opacity': 0})),
            state('1', style({'fill-opacity': 1, 'stroke-opacity': 1}))
            //            transition('1 => 0', animate('300ms')),
            //            transition('0 => 1', animate('300ms')),
            //transition('* => *', animate('.5s'))
        ]),
    ]
})
export class KettlePotComponent implements OnInit {
    @Input() status: boolean;
    @Input() width: number;
    @Input() height: number;
    @Input() level: boolean;
    @Input() boiling: boolean;

    boilLines: any = [];

    bubbleTweens: any = [];
    bubbleLines: any = [];

    // show control UI widgets
    debug: boolean = false;

    timeBoil1 = new TimelineMax({repeat: -1, repeatDelay: 0.5});

    constructor( @Inject(ElementRef) private elementRef: ElementRef) {
    }


    startBoiling() {
        this.timeBoil1.play();
        for (let x = 0; x < this.boilLines.length; x++) {
            this.boilLines[x].style['stroke-opacity'] = 1.0;
        }
        for (let x = 0; x < this.bubbleLines.length; x++) {
            this.bubbleLines[x].style['fill-opacity'] = 1.0;
        }
    }

    stopBoiling() {
        this.timeBoil1.stop();
        for (let x = 0; x < this.boilLines.length; x++) {
            console.log(this.boilLines[x]);
            this.boilLines[x].style['stroke-opacity'] = 0;
        }
        for (let x = 0; x < this.bubbleLines.length; x++) {
            this.bubbleLines[x].style['fill-opacity'] = 0;
        }
    }

    ngOnInit() {


        let group = this.elementRef.nativeElement;
        let lines = group.querySelectorAll("path");

        // extract paths that have an id starting with boil
        for (let x = 0; x < lines.length; x++) {
            if (lines[x].id.startsWith("boil")) {
                this.boilLines.push(lines[x]);
            }
            if (lines[x].id.startsWith("bubble")) {
                this.bubbleLines.push(lines[x]);
            }
        }

        this.timeBoil1
            .set(this.boilLines, {drawSVG: "0% 0%"})
            .to(this.boilLines[0], 0.5, {drawSVG: "100% 0%"})
            .to(this.boilLines[1], 0.5, {drawSVG: "0% 100%"});


        if (this.bubbleLines.length === 3) {

            this.bubbleTweens.push(TweenMax.to(this.bubbleLines[0], 2, {y: -220, repeat: -1}));
            this.bubbleTweens.push(TweenMax.to(this.bubbleLines[0], .5, {x: "25", repeat: -1, yoyo: true}));

            this.bubbleTweens.push(TweenMax.to(this.bubbleLines[1], 1.5, {y: -220, repeat: -1}));
            this.bubbleTweens.push(TweenMax.to(this.bubbleLines[1], .5, {x: "-10", repeat: -1, yoyo: true}));

            this.bubbleTweens.push(TweenMax.to(this.bubbleLines[2], 1.0, {y: -220, repeat: -1}));
            this.bubbleTweens.push(TweenMax.to(this.bubbleLines[2], .5, {x: "5", repeat: -1, yoyo: true}));

        }

    }
}


export const COMPONENTS = [
    KettleComponent,
    KettlePotComponent
];