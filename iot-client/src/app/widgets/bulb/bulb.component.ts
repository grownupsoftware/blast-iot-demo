import {Component, OnInit, Input, Output, EventEmitter, ChangeDetectionStrategy} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {MdSlider, MdSlideToggle, MdSlideToggleChange} from "@angular/material";
import {trigger, state, style, transition, animate} from '@angular/animations';

@Component({
    moduleId: module.id,
    selector: 'bulb',
    template: `
    <div [ngStyle]="{width: width+'px',height: height+'px'}">
        <svg xmlns="http://www.w3.org/2000/svg"  width="100%" height="100%" viewBox="0 0 34 60">
            <svg:g style="fill:#797878" transform="translate(-16,-3)">
                <svg:path d="m 37.25,50 -10.5,0 c -0.55,0 -1,-0.45 -1,-1 0,-0.55 0.45,-1 1,-1 l 10.5,0 c 0.55,0 1,0.45 1,1 0,0.55 -0.45,1 -1,1"/>
                <svg:path d="m 37.25,53 -10.5,0 c -0.55,0 -1,-0.45 -1,-1 0,-0.55 0.45,-1 1,-1 l 10.5,0 c 0.55,0 1,0.45 1,1 0,0.55 -0.45,1 -1,1"/>
                <svg:path d="m 37.07,56 -10.14,0 c -0.65,0 -1.18,-0.53 -1.18,-1.18 0,-0.45 0.37,-0.82 0.82,-0.82 l 10.85,0 c 0.45,0 0.82,0.37 0.82,0.82 0.01,0.65 -0.52,1.18 -1.17,1.18"/>
                <svg:path d="m 32,60 c 2.21,0 4,-1.34 4,-3 l -8,0 c 0,1.66 1.79,3 4,3"/>
            </svg:g>
            <svg:path class="light-color" transform="translate(0,2)" [@visibilityChanged]="status" style="stroke:#000000;stroke-width:0.5" d="M 32,15.58 C 32,6.97 24.84,0 16,0 7.16,0 0,6.97 0,15.58 c 0,2.73 0.72,5.29 1.98,7.51 0.77,1.49 1.32,2.56 1.67,3.23 0.72,1.4 1.14,2.21 1.66,3.42 0.22,0.51 0.55,1.28 0.86,2.16 1.23,3.49 1.7,6.82 1.7,6.82 0,0 0.23,1.74 1.16,2.59 0.26,0.24 0.52,0.38 0.65,0.44 0.45,0.21 0.86,0.25 1.11,0.25 0.16,0 0.32,0 0.48,0 l 4.66,0 0.13,0 4.66,0 c 0.16,0 0.32,0 0.48,0 0.24,0 0.66,-0.04 1.11,-0.25 0.13,-0.06 0.39,-0.2 0.65,-0.44 0.93,-0.85 1.16,-2.59 1.16,-2.59 0,0 0.47,-3.33 1.7,-6.82 0.31,-0.88 0.64,-1.65 0.86,-2.16 0.52,-1.21 0.94,-2.02 1.66,-3.42 0.35,-0.68 0.9,-1.74 1.67,-3.23 C 31.28,20.86 32,18.3 32,15.58"/>
        </svg>
    </div>
     `,
    styles: [],

    animations: [
        trigger('visibilityChanged', [
            state('1', style({'fill-opacity': 1, fill: '#ffc84d'})),
            state('0', style({'fill-opacity': 0.1, fill: '#eee'})),
            transition('1 => 0', animate('1300ms')),
            transition('0 => 1', animate('1300ms')),
            //transition('* => *', animate('.5s'))
        ])
        //        trigger('switchOn', [
        //            state('on', style({display: 'show'})),
        //            state('off', style({display: 'none'}))
        //            //            transition('off => on', animate('600ms')),
        //            //            transition('on => off', animate('600ms')),
        //            //transition('* => *', animate('.5s'))
        //        ]),
        //        trigger('switchOff', [
        //            state('on', style({display: 'none'})),
        //            state('off', style({display: 'show'}))
        //            //            transition('off => on', animate('600ms')),
        //            //            transition('on => off', animate('600ms')),
        //            //transition('* => *', animate('.5s'))
        //        ])
    ]
})
export class BulbComponent {
    @Input() status: boolean;
    @Input() width: number;
    @Input() height: number;
}