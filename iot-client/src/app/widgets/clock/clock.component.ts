import {Component, OnInit, Input, AfterViewInit, Output, EventEmitter, ChangeDetectionStrategy, ReflectiveInjector, Inject, ElementRef, ViewChild, ChangeDetectorRef} from '@angular/core';
import {MdSlider, MdSlideToggle, MdSlideToggleChange, MdSliderChange} from "@angular/material";
import {trigger, state, style, transition, animate} from '@angular/animations';
import {Observable} from 'rxjs/Observable';
import {DataService} from '../../services/data.service';
import {Attachment} from 'blast-graph-angular2/blast';

import "gsap";
//const TweenMax = require('gsap').TweenMax;
//const Cubic = require('gsap').Cubic;
declare var TimelineMax, ease, TimelineMax, TweenMax, Cubic, Power4, Power1, Power2, Power3, Bounce, Elastic: any;

// common function to set time
const setTime = function (dateTime: string, elementRef: ElementRef, showSeconds: boolean) {
    let now: Date;
    if (dateTime === undefined) {
        now = new Date();
    } else {
        now = new Date(dateTime);
    }

    let midnight: Date = new Date();
    midnight.setHours(0);
    midnight.setMinutes(0);
    midnight.setSeconds(0);

    let seconds = (now.getTime() - midnight.getTime()) / 1000;
    let minutes = seconds / 60;
    let hours = minutes / 60;
    let dayNumber = now.getDate();

    let elements = elementRef.nativeElement.querySelectorAll("g");

    for (let x = 0; x < elements.length; x++) {
        if (elements[x].id === 'offset-hours') {
            elements[x].style.transform = 'rotate(' + (hours % 12 / 12 * 360) + 'deg)';
        }
        if (elements[x].id === 'offset-minutes') {
            elements[x].style.transform = 'rotate(' + (minutes / 60 * 360) + 'deg)';
        }
        if (elements[x].id === 'offset-seconds') {
            if (showSeconds === true) {
                elements[x].style.transform = 'rotate(' + (seconds / 60 * 360) + 'deg)';
            } else {
                elements[x].style.display = 'none';
            }
        }
    }

    let texts = elementRef.nativeElement.querySelectorAll("text");
    for (let x = 0; x < texts.length; x++) {
        if (texts[x].id === 'date-value') {
            texts[x].innerHTML = dayNumber;
        }
    }
}


@Component({
    moduleId: module.id,
    selector: 'local-clock',
    changeDetection: ChangeDetectionStrategy.OnPush,
    templateUrl: './clock.component.html',
    styleUrls: ['./local-clock.component.css']
})
export class LocalClockComponent implements AfterViewInit {
    title: string = 'Local';
    time: string = undefined;
    showSeconds: boolean = true;

    constructor( @Inject(ElementRef) private elementRef: ElementRef) {}

    ngAfterViewInit() {
        setTime(this.time, this.elementRef, this.showSeconds);
    }
    
    updateTime(): boolean {
        // for ticker allows sec,hrs etc to be updated
        return true;
    }
}

@Component({
    moduleId: module.id,
    selector: 'ticker-clocks',
    //changeDetection: ChangeDetectionStrategy.Default,
    template: `
    <div class="ticker-clocks">
            <ticker-clock class="ticker-clock" *ngFor="let clock of tickers" [title]="clock.title" [time]="clock.time" [showSeconds]="false"></ticker-clock>"
            <local-clock class="ticker-clock"></local-clock>
    <div>
    `,
    styles: ['.ticker-clocks { display:inline-block;width:100%}',
        '.ticker-clock { display:inline-block;width:150px;height:150px;padding:10px;}'
    ]
})
export class TickerClocksComponent {
    tickers: Array<any> = [];
    attachment: Attachment;

    constructor( @Inject(DataService) private dataService: DataService,
        private changeDetector: ChangeDetectorRef) {

        this.attachment = this.dataService.getBlastService().attach('tickers', this.tickers);

        //        // if the data changes, then mark as changed
        //        this.attachment.loadStream().subscribe((data) => {
        //            console.log('loaded', data);
        //            this.changeDetector.markForCheck();
        //        });
        //        this.attachment.addedStream().subscribe((data) => {
        //            console.log('added', data);
        //            this.changeDetector.markForCheck();
        //        });
        //        this.attachment.changedStream().subscribe((data) => {
        //            console.log('Changed', data);
        //            //this.changeDetector.markForCheck();
        //        });
        //        this.attachment.removedStream().subscribe((data) => {
        //            this.changeDetector.markForCheck();
        //        });

    }
}
@Component({
    moduleId: module.id,
    selector: 'ticker-clock',
    changeDetection: ChangeDetectionStrategy.Default,
    templateUrl: './clock.component.html',
    styleUrls: ['./clock.component.css']
})
export class TickerClockComponent implements AfterViewInit {
    @Input() title: string;
    @Input() time: string;
    @Input() showSeconds: boolean = true;

    constructor( @Inject(ElementRef) private elementRef: ElementRef) {
    }
    
    ngAfterViewInit() {
        setTime(this.time, this.elementRef, this.showSeconds);
    }

    updateTime(): boolean {
      // update secs,mins, hrs
      setTime(this.time, this.elementRef, this.showSeconds);
        return true;
    }

}

export const COMPONENTS = [
    TickerClocksComponent,
    TickerClockComponent,
    LocalClockComponent
];