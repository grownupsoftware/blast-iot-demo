import {Component} from '@angular/core';

@Component({
    moduleId: module.id,
    selector: 'gauge',
    template: `
    <div>Im a gauge</div>
    `,
    styles: []
})
export class GaugeComponent {
    data: any = {};

    constructor() {
    }

    setData(data: any) {
        this.data = data;
    }
}