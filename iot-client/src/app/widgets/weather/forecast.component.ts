import {Component, OnInit, Input, Output, EventEmitter, ChangeDetectionStrategy} from '@angular/core';
import {Observable} from 'rxjs/Observable';

@Component({
    moduleId: module.id,
    selector: 'weather-forecast',
    templateUrl: './forecast.component.html',
    styles: []
})
export class WeatherForecastComponent implements OnInit {
    data: any = {};

    constructor() {
    }

    setData(data: any) {
        this.data = data;
    }

    ngOnInit() {
        console.log('--------------- called');
    }
}