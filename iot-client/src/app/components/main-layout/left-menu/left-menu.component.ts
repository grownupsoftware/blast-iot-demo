import {Inject, Component, OnInit} from '@angular/core';
import {DataService} from '../../../services/data.service'
import {Transition} from '@uirouter/angular';
import {Store, provideStore} from '@ngrx/store';

export class ToggleDO {
    open: boolean;
    constructor() {
        this.open = true;
    }
}

@Component({
    moduleId: module.id,
    selector: 'left-menu',
    templateUrl: './left-menu.component.html',
    styleUrls: ['./left-menu.component.css'],
})
export class LeftMenuComponent implements OnInit {
    data: any = {};
    byLocation: any = [];
    byCategory: any = [];
    toggles: any = {rooms: true, category: false, info: true}

    constructor( @Inject(DataService) dataService: DataService, public transition: Transition) {
        dataService.attachData(this.data);
        this.byLocation = this.groupBy(this.data['things'], 'location');
        this.byCategory = this.groupBy(this.data['things'], 'category');

        console.log('data->', this.data);
        console.log('menu->', this.byLocation);
    }

    groupBy(data: Array<any>, propertyName: string): Array<any> {

        const groups: any = {};
        for (var i = 0; i < data.length; i++) {
            var groupName = data[i][propertyName];
            if (!groups[groupName]) {
                groups[groupName] = [];
            }
            groups[groupName].push(data[i]);
        }
        const myArray = [];
        for (const groupName in groups) {
            myArray.push({name: groupName, things: groups[groupName]});
        }
        return myArray
    }
    ngOnInit() {
    }

    onCategoryClick(category: any, $event: any) {
        const state = this.transition.router.stateService;
        console.log('clicked', 'name', category.name);
        $event.stopPropagation();
        return state.go('home.category', {title: category.name}, {custom: category.things});
    }

    onLocationClick(location: any, $event: any) {
        console.log('clicked', 'location', location.name, location.things);
        const state = this.transition.router.stateService;
        $event.stopPropagation();
        return state.go('home.room', {title: location.name}, {custom: location.things});
    }

    onInfoClick(key: string, $event: any) {
        console.log('clicked', 'info', key, this.data.info[key].data);
        const state = this.transition.router.stateService;
        $event.stopPropagation();
        return state.go('home.info', {title: this.data.info[key].name, key: key}, {custom: this.data.info[key]});

    }
    isSectionOpen(label: string) {
        return this.toggles[label];

    }
    getToggleSectionIcon(label: string) {
        return this.toggles[label] ? 'fa fa-caret-down' : 'fa fa-caret-right';
    }

    toggleSection(label: string, $event: any) {
        this.toggles[label] = !this.toggles[label];
        $event.stopPropagation();
    }

    getKeys(data: any): string[] {
        let keys: string[] = [];
        for (var k in data) {
            if (data.hasOwnProperty(k)) {
                //if (Array.isArray(data[k])) {
                keys.push(k);
                //}
            }
        }
        return keys;
    }
}
