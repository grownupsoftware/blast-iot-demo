import {Component, OnInit, Inject} from '@angular/core';
import {DataService} from '../../../services/data.service'

@Component({
    moduleId: module.id,
    selector: 'right-menu',
    templateUrl: './right-menu.component.html',
    styleUrls: ['./right-menu.component.css']
})
export class RightMenuComponent implements OnInit {

    constructor( @Inject(DataService) public dataService: DataService) {
    }

    ngOnInit() {
    }

    logout() {
        this.dataService.logout();
    }

}
