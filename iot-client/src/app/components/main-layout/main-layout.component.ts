import {Component, OnInit, Inject} from '@angular/core';
import {trigger, state, style, transition, animate} from '@angular/animations';

export const colors: any = {
    red: {
        primary: '#ad2121',
        secondary: '#FAE3E3'
    },
    blue: {
        primary: '#1e90ff',
        secondary: '#D1E8FF'
    },
    yellow: {
        primary: '#e3bc08',
        secondary: '#FDF1BA'
    }
};

@Component({
    moduleId: module.id,
    selector: 'main-layout',
    //templateUrl: './main-layout.component.html',
    template : '<layout></layout>',
    //templateUrl: './main-layout.sample.html',
    
    styleUrls: ['./main-layout.component.css'],

    animations: [
        trigger('slideRightInOut', [
            state('show', style({
                transform: 'translate3d(0, 0, 0)'
            })),
            state('hide', style({
                transform: 'translate3d(100%, 0, 0)'
            })),
            transition('show => hide', animate('400ms ease-in-out')),
            transition('hide => show', animate('400ms ease-in-out'))
        ]),
        trigger('slideLeftInOut', [
            state('show', style({
                'webkit-transform': 'translate3d(0, 0, 0)',
                transform: 'translate3d(0, 0, 0)'
            })),
            state('hide', style({
                'webkit-transform': 'translate3d(-250px, 0, 0)',
                transform: 'translate3d(-250px, 0, 0)'
            })),
            transition('show => hide', animate('400ms ease-in-out')),
            transition('hide => show', animate('400ms ease-in-out'))
        ]),
        trigger('mainContent', [
            state('show', style({
                'webkit-transform': 'translate3d(250px, 0, 0)',
                transform: 'translate3d(250px, 0, 0)'
            })),
            state('hide', style({
                'webkit-transform': 'translate3d(0px, 0, 0)',
                transform: 'translate3d(0, 0, 0)'
            })),
            transition('show => hide', animate('400ms ease-in-out')),
            transition('hide => show', animate('400ms ease-in-out'))
        ]),
    ]
})
export class MainLayoutComponent implements OnInit {
    menuRightState: string = 'hide';
    menuLeftState: string = 'show';
    title = 'app works!';
    username: string;

color = 'primary';
  mode = 'determinate';
  value = 50;

    ngOnInit() {
    }

    toggleRightMenu() {
        this.menuRightState = this.menuRightState === 'hide' ? 'show' : 'hide';
        this.menuLeftState = 'hide';
    }
    toggleLeftMenu() {
        this.menuLeftState = this.menuLeftState === 'hide' ? 'show' : 'hide';
        this.menuRightState = 'hide';
    }

    closeAllMenus() {
        this.menuLeftState = 'hide';
        this.menuRightState = 'hide';
    }

    getHeight = function () {
        return {
            height: (window.innerHeight - 120) + 'px',
            'background-color': 'red'
        };
    };

foods = [
    {value: 'steak-0', viewValue: 'Steak'},
    {value: 'pizza-1', viewValue: 'Pizza'},
    {value: 'tacos-2', viewValue: 'Tacos'}
  ];
  
  folders = [
      {name:'fred',updated:false},
      {name:'joe',updated:false},
  ];

  notes = [
      {name:'note1',updated:false},
      {name:'note2',updated:false},
  ]
 tiles = [
    {text: 'One', cols: 3, rows: 1, color: 'lightblue'},
    {text: 'Two', cols: 1, rows: 2, color: 'lightgreen'},
    {text: 'Three', cols: 1, rows: 1, color: 'lightpink'},
    {text: 'Four', cols: 2, rows: 1, color: '#DDBDF1'},
  ];
}
