import {NgModule} from '@angular/core';

import {ZUnderlineDirective} from './z-underline.directive';
import {AnimateFillDirective} from './animate-fill.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';


const COMPONENTS: any = [
];

const DIRECTIVES: any = [
    ZUnderlineDirective,
    AnimateFillDirective
];

@NgModule({
    imports: [BrowserAnimationsModule],
    exports: [...COMPONENTS, ...DIRECTIVES],
    declarations: [...COMPONENTS, ...DIRECTIVES]
})
export class SVGModule {}