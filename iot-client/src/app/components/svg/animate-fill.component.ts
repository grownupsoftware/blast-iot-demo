import {Component, OnInit, Inject, ElementRef, Input, HostBinding} from '@angular/core';
@Component({
    moduleId: module.id,
    selector: `[animate-fill]`,
    template: 'animate-fill: {{level}}'
})
export class AnimateFillDirective implements OnInit {
    @Input() level: number = 0;

    constructor( @Inject(ElementRef) private elementRef: ElementRef) {
    }

    ngOnInit() {
        console.log('--->',this.level);
        this.elementRef.nativeElement.setAttribute('offset', this.level + '%');
    }
//    @HostBinding('offset')
//    public get offset(): string {
//        console.log('getting offset');
//        return this.level + '%';
//    }
//    
//@HostListener("focus", ["$event.target.value"])
//  onFocus(value) {
//    this.el.value = this.currencyPipe.parse(value); // oposite of transform
//    this.ngModelChange.emit(this.el.value);
//  }
//
//  @HostListener("blur", ["$event.target.value"])
//  onBlur(value) {
//    this.el.value = this.cleanNumber(value); //"987654" ;//this.currencyPipe.transform(value);
//    this.ngModelChange.emit(this.el.value);
//  }
    
}
