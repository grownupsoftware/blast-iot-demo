import {Component, OnInit, Inject, ElementRef} from '@angular/core';
import "gsap";
//const TweenMax = require('gsap').TweenMax;
//const Cubic = require('gsap').Cubic;
declare var TimelineMax, ease, TimelineMax, TweenMax, Cubic, Power4, Power1, Power2, Power3, Bounce, Elastic: any;

@Component({
    moduleId: module.id,
    selector: `[z-underline]`,
    template: `
    <svg:path class="line1 z-underline" [attr.d]="path1"></svg:path>
    <svg:path class="line2 z-underline" [attr.d]="path2"></svg:path>
    <svg:path class="line3 z-underline" [attr.d]="path3"></svg:path>`,
    styleUrls: ['./z-underline.component.css']
})
export class ZUnderlineDirective implements OnInit {

    path1 = "M4.2,562 c120.5,0,231.7,2.7,355,1.4";
    path2 = "M359.3,563.5c-123.5,2.9-237.1,4.8-357.5,11.8";
    path3 = "M2.8,575.2c105.2-0.7,168-3.7,352.9-1.2";
    timeline = new TimelineMax({repeat: -1, repeatDelay: 1.5});

    constructor( @Inject(ElementRef) private elementRef: ElementRef) {}

    ngOnInit() {

        let group = this.elementRef.nativeElement;
        let lines = group.querySelectorAll("path");

        this.timeline
            .set(lines, {drawSVG: "0% 0%"})
            .to(lines[0], 0.2, {drawSVG: "100% 0%"})
            .to(lines[1], 0.2, {drawSVG: "0% 100%"})
            .to(lines[2], 0.1, {drawSVG: "0% 100%"})
            .staggerTo(lines, 1, {autoAlpha: 0}, 0.25, "+=1.5");
    }
}
