import {Inject, Component} from '@angular/core';
import {Transition} from '@uirouter/angular';

import {DataService} from '../../services/data.service';

@Component({
    moduleId: module.id,
//    selector: 'room',
    template: `
    <div>room</div>
    `,
    styles: []
})
export class RoomComponent {
    title: string;
    data: any;

    constructor( @Inject(DataService) dataService: DataService, transition: Transition) {
        console.log('params', transition.options().custom);
        this.title = transition.params().title;
        this.data = transition.options().custom;
        console.log('data=>', this.data);
//        if (this.data === undefined || Object.keys(this.data).length === 0) {
//            // if no event object go back to home, we don't have all the data
//            console.log('going back to home');
//            transition.router.stateService.go('home');
//        }
    }

}