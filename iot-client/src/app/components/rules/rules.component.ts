import {Component, Input, Output, EventEmitter, ChangeDetectionStrategy} from '@angular/core';
import {Observable} from 'rxjs/Observable';

import {Rule, RulesStoreService, RuleSelectedStoreService} from '../../services/store/application-store.module';


@Component({
    moduleId: module.id,
    selector: 'rules',
    template: `
{{rules | async | json}}
    <rule-filter (updateFilter)="updateFilter($event)"></rule-filter>    
    <div  class="mdl-grid items">
      <div class="mdl-cell mdl-cell--6-col">
         <rules-list [rules]="rules | async" (selected)="select($event)" (deleted)="remove($event)"></rules-list>
      </div>   
    </div>
    <div  class="mdl-cell mdl-cell--6-col">
      <rule-detail [rule]="selectedRule | async" (saved)="save($event)" (cancelled)="reset($event)"></rule-detail>
    </div>
    `,
    styles: [],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class RulesComponent {
    rules: Observable<Array<Rule>>;
    selectedRule: Observable<Rule>;

    constructor(private rulesService: RulesStoreService, private ruleSelectedStoreService: RuleSelectedStoreService) {

        // link and attach rules
        this.rules = rulesService.rules;
        rulesService.attach();

        // link selected rule
        this.selectedRule = ruleSelectedStoreService.selectedRule;

        //this.rules = store.select(RULE_STORE_NAME);
        //this.selectedRule = store.select(SELECTED_RULE_STORE_NAME);

        //        this.rules = Observables.combineLatest(
        //            _store.select(RULE_STORE_NAME),
        //            _store.select(RULE_FILTER_NAME),
        //            (rules, filter) => {
        //                rules.filter(filter);
        //            }
        //        )
        //        _store.select<Rule[]>('rules')
        //            .subscribe(rules => {
        //                this.rules = rules;
        //            })
    }

    select(rule: Rule) {
        this.ruleSelectedStoreService.select(rule);
    }

    reset() {
        this.ruleSelectedStoreService.reset();
    }

    remove(rule: Rule) {
        this.rulesService.remove(rule);
    }

    // ---------- keep
    // -- example observer for update
    //        this.rulesService.update(rule).subscribe(action => {
    //            console.log('=====> all done', action);
    //        });

    save(rule: Rule) {
        this.rulesService.save(rule);
        this.reset();
    }

    updateFilter(filter) {
        //        this._store.dispatch({type: filter});
    }
}

// ------------------------------------------------------------------------- rules filter

@Component({
    selector: 'rule-filter',
    template: `
      <div class="margin-bottom-10">
        <select #selectList (change)="updateFilter.emit(selectList.value)">
            <option *ngFor="let filter of filters" value="{{filter.action}}">
                {{filter.friendly}}
            </option>
        </select>
      </div>
    `
})
export class RuleFilterComponent {
    public filters = [
        {friendly: "All", action: 'SHOW_ALL'},
        {friendly: "Attending", action: 'SHOW_ATTENDING'},
        {friendly: "Guests", action: 'SHOW_WITH_GUESTS'}
    ];
    @Output() updateFilter: EventEmitter<string> = new EventEmitter<string>();
}

// ------------------------------------------------------------------------- rules list
@Component({
    moduleId: module.id,
    selector: 'rules-list',
    template: `
        <div *ngFor="let rule of rules" (click)="selected.emit(rule)"  class="fem-card mdl-card mdl-shadow--2dp">
            <div  class="mdl-card__title">
                <h2  class="mdl-card__title-text">{{rule.name}}</h2>
            </div>
            <div  class="mdl-card__supporting-text">
                {{rule.description}}
            </div>
            <div  class="mdl-card__menu">
                <button (click)="deleted.emit(rule); $event.stopPropagation();" class="mdl-button mdl-button--icon mdl-js-button mdl-js-ripple-effect">
                    <i class="material-icons">close</i>
                </button>    
            </div>
        </div>
    `,
    styles: [],
})
export class RulesListComponent {
    @Input() rules: Rule[];
    @Output() selected = new EventEmitter();
    @Output() deleted = new EventEmitter();
}

// ------------------------------------------------------------------------- rules detail
@Component({
    moduleId: module.id,
    selector: 'rule-detail',
    template: `
    {{selectedRule | json}}
        <div  class="fem-card mdl-card mdl-shadow--2dp">
            <div  class="mdl-card__title">
                <h2  class="mdl-card__title-text" *ngIf="selectedRule.id">Editing {{originalName}}</h2>
                <h2  class="mdl-card__title-text" *ngIf="!selectedRule.id">Create New Item</h2>
            </div>
            <div  class="mdl-card__supporting-text">
                <form novalidate>
                    <div class="mdl-textfield mdl-js-textfield">
                        <label>Rule Name</label>
                        <input [(ngModel)]="selectedRule.name" name="name"
                           placeholder="Enter a name" type="text">
                     </div>
                    <div class="mdl-textfield mdl-js-textfield">
                        <label>Rule Description</label>
                        <input [(ngModel)]="selectedRule.description" name="description"
                           placeholder="Enter a description" type="text">
                    </div>
                    <div  class="mdl-card__actions">
                          <button type="button" (click)="cancelled.emit(selectedRule)" class="mdl-button mdl-js-button mdl-js-ripple-effect">Cancel</button>
                          <button type="submit" (click)="saved.emit(selectedRule)" class="mdl-button mdl-js-button mdl-button--colored mdl-js-ripple-effect">Save</button>
                    </div>        
                </form>
            </div>
        </div>    
    `,
    styles: [],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class RuleDetailComponent {
    originalName: string;
    selectedRule: Rule;

    @Input('rule') set rule(value: Rule) {
        if (value) {
            this.originalName = value.name;
        }
        this.selectedRule = Object.assign({}, value);
    }
    @Output() saved = new EventEmitter();
    @Output() cancelled = new EventEmitter();
}

// ------------------------------------------------------------------------- all components
export const COMPONENTS = [
    RulesComponent,
    RuleFilterComponent,
    RulesListComponent,
    RuleDetailComponent
];
