import {OnInit, Component} from '@angular/core';
import {Transition} from '@uirouter/angular';

@Component({
    moduleId: module.id,
    selector: 'info',
    template: `
    <div>{{title}}</div>
    <widget [tag]="key" [data]="data"></widget>    
    `,
    styles: []
})
export class InfoComponent implements OnInit {
    title: string;
    key: string;
    data: any;

    constructor(private transition: Transition) {
        console.log('custom option', transition.options().custom);
        console.log('params', transition.params());
        this.title = transition.params().title;
        this.key = transition.params().key;
        this.data = transition.options().custom;
        console.log('data=>', this.data);
    }

    ngOnInit() {
        //        console.log('============= called');
        //        if (this.data === undefined || Object.keys(this.data).length === 0) {
        //            // if app starts on this page we have no data so go back home
        //            console.log('going back to home - no data');
        //            this.transition.router.stateService.go('home');
        //        }
    }
}