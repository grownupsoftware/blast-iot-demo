import {Component, ViewChild, ElementRef, OnInit} from '@angular/core';
import "gsap";
//const TweenMax = require('gsap').TweenMax;
//const Cubic = require('gsap').Cubic;
declare var ease, TimelineMax,TweenMax,Cubic,Power4,Power1,Power2,Power3,Bounce, Elastic:any;
declare var $:any;
@Component({
    moduleId: module.id,
    selector: 'logo',
//    templateUrl : 'logo.component.html',
//    templateUrl : 'writing.component.html',
    template: `
    <div class="logo">
        <svg viewBox="0 0 250 250">
          <svg:polygon #left class="left" points="125,30 125,30 125,30 31.9,63.2 46.1,186.3 125,230 125,230 125,230 203.9,186.3 218.1,63.2" />
          <svg:polygon #right class="right" points="125,30 125,52.2 125,52.1 125,153.4 125,153.4 125,230 125,230 203.9,186.3 218.1,63.2 125,30" />
          <svg:path class="a" d="M125,52.1L66.8,182.6h0h21.7h0l11.7-29.2h49.4l11.7,29.2h0h21.7h0L125,52.1L125,52.1L125,52.1L125,52.1L125,52.1z M142,135.4H108l17-40.9L142,135.4z"/>
        </svg>
    </div>
//    `,
    styleUrls: ['./logo.component.css']
//    styleUrls: ['./writing.component.css']
//    styles: [
//        '.logo { width:300px;height:300px;}',
//        'svg {display: block;max-width: 300px;margin: 0 auto;}',
//        '#left, .shield { fill:#DD0031;}',
//        '#right { fill:#C3002F;}',
//        '.a { fill:#FFFFFF;}',
//    ]
})
export class LogoComponent implements OnInit {
    @ViewChild('left') left: ElementRef;
    @ViewChild('right') right: ElementRef;
    @ViewChild('sig') sig: ElementRef;

    ngOnInit() {
        TweenMax.to(this.left.nativeElement, 1, {
            attr: {
                points: '125,30 125,30 125,30 31.9,30 31.9,230 125,230 125,230 125,230 203.9,186.3 218.1,63.2'
            },
            repeat: -1,
            yoyo: true,
            ease: Cubic.easeInOut
        });
        TweenMax.to(this.right.nativeElement, 1, {
            attr: {
                points: '125,30 125,52.2 125,52.1 125,153.4 125,153.4 125,230 125,230 218.1,230 218.1,30 125,30'
            },
            repeat: -1,
            yoyo: true,
            ease: Cubic.easeInOut
        });
        
//TweenMax.staggerFromTo(this.sig.nativeElement, 0.8, {drawSVG: "0%"}, {drawSVG: "100%"}, 0.8);
//TweenMax.staggerFromTo($('#mask-sig path'), 0.4, {drawSVG: "0%"}, {drawSVG: "100%"}, 0.4);
        
    }
}