export class DeviceDO {
    id: number;
    title: string;
    category: string;
    data: any;
}