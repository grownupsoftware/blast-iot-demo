import {NgModule} from '@angular/core';
import {ModuleWithProviders} from '@angular/core';
import {StoreModule} from '@ngrx/store';
import {EffectsModule} from '@ngrx/effects';

import {RulesStoreService} from './rules/rules-store.service';
import {RuleSelectedStoreService} from './rules/rule-selected-store.service';
import {RulesFilterService} from './rules/rules-filter.service';
/**
 * add each store to STORES
 */
const STORES: any = {};
STORES[RulesStoreService.storeName()] = RulesStoreService.reducer();
STORES[RuleSelectedStoreService.storeName()] = RuleSelectedStoreService.reducer();
//STORES[RulesFilterService.storeName()] = RulesFilterService.reducer();

console.log('STORES', STORES);


/**
 * add each store service to providers
 */
const PROVIDERS: Array<{provide: any, useClass: any}> = [
    {provide: RulesStoreService, useClass: RulesStoreService},
    {provide: RuleSelectedStoreService, useClass: RuleSelectedStoreService},
    //{provide: RulesFilterService, useClass: RulesFilterService}
];

export const APPLICATION_STORES: ModuleWithProviders = StoreModule.provideStore(STORES);

@NgModule({
    declarations: [],
    imports: [APPLICATION_STORES,
        EffectsModule.run(RulesStoreService)
    ],
    providers: [
        ...PROVIDERS
    ]
})
export class ApplicationStoreModule {}
export {RulesStoreService};
export {RuleSelectedStoreService};
export {RulesFilterService};
export {Rule} from './rules/rule';

