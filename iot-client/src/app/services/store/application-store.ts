import {ModuleWithProviders} from '@angular/core';
import {StoreModule} from '@ngrx/store';

import {Rule} from './rule-store';
//import {RULE_STORE_NAME, RulesReducer} from './rule-store';
import {RulesStoreService} from './rules/rules-store.service';

const STORES: any = {};

/**
 * add each store to STORES
 */
STORES[RulesStoreService.storeName()] = RulesStoreService.reducer();


console.log('STORES', STORES);

export interface ApplicationStore {
    rules: Rule[];
    selectedRule: Rule;
}

export const APPLICATION_STORES: ModuleWithProviders = StoreModule.provideStore(STORES);

//export const APPLICATION_STORES : ModuleWithProviders = StoreModule.provideStore({
//    rules: RulesReducer
//});
