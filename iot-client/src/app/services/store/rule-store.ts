import {Action} from '@ngrx/store';

export interface Rule {
    id: number;
    name: string;
    description: string;
}

// ------------------------------------------------------ rule
export const RULE_STORE_NAME = 'rules';

export const RULE_ACTION = {
    ADD_RULES: 'ADD_RULES',
    ADD_RULE: 'ADD_RULE',
    EDIT_RULE: 'EDIT_RULE',
    REMOVE_RULE: 'REMOVE_RULE',
}

export const RulesReducer = (state: Rule[] = [], action: Action) => {
    switch (action.type) {
        case RULE_ACTION.ADD_RULES:
            console.log('adding rules', action.payload);
            return action.payload;

        case RULE_ACTION.ADD_RULE:
            return [
                ...state,
                action.payload
            ];
        case RULE_ACTION.EDIT_RULE:
            return state.map(rule => {
                if (rule.id === action.payload.id) {
                    return Object.assign({}, rule, action.payload);
                }
            });
        case RULE_ACTION.REMOVE_RULE:
            return state.filter(rule => rule.id !== action.payload);
        default:
            return state;
    }
}

//APPLICATION_REDUCERS[RULE_STORE_NAME] = RulesReducer;
// ------------------------------------------------------ rule filter

export const RULE_FILTER_NAME = 'ruleFilters'

export const RULE_FILTER = {
    ALL: 'ALL',
    ONLY_LIGHTS: 'ONLY_LIGHTS'
}

export const RuleFilterReducer = (state = rule => rule, action) => {
    switch (action.type) {
        case RULE_FILTER.ALL:
            return rule => rule;
        case RULE_FILTER.ONLY_LIGHTS:
            return rule => rule.id === 1;
        default:
            return state;
    }
}
//APPLICATION_REDUCERS[RULE_STORE_NAME] = RuleFilterReducer;

// ------------------------------------------------------ selected rule
export const SELECTED_RULE_STORE_NAME = 'selectedRule';

export const SELECTED_RULE_ACTION = {
    SELECT_RULE: 'SELECT_RULE'
}

export const SelectedRuleReducer = (state: any = null, {type, payload}) => {
    switch (type) {
        case SELECTED_RULE_ACTION.SELECT_RULE:
            return payload;
        default:
            return state;
    }
};

//APPLICATION_REDUCERS[RULE_STORE_NAME] = SelectedRuleReducer;

// ------------------------------------------------------ add here
