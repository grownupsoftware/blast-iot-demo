import {Injectable} from '@angular/core';
import {Store, Action} from '@ngrx/store';
import {Observable} from 'rxjs/Observable';

import {IStore} from '../IStore';
import {Rule} from './rule';
import {ApplicationStore} from '../application-store';

const NAME: string = 'ruleFilters';

// prefix all actions with NAME - dispatches are actioned on all stores, so need to differentiate
const ALL: string = NAME + '_' + 'ALL';
const ONLY_LIGHTS: string = NAME + '_' + 'ONLY_LIGHTS';

//const REDUCER = (state = rule => rule, action: {type: string, payload: Rule}) => {
const REDUCER = (state = rule => rule, action: Action) => {
    console.log('rules-filter', action);
    switch (action.type) {
        case ALL:
            return rule => rule;
        case ONLY_LIGHTS:
            return rule => rule.id === 1;
        default:
            return state;
    }
}

@Injectable()
export class RulesFilterService implements IStore {
    selectedRule: Observable<Rule>;

    constructor(private store: Store<ApplicationStore>) {
        this.selectedRule = store.select(NAME);
    }

    public static storeName(): string {
        return NAME;
    }

    public static reducer() {
        return REDUCER;
    }

}
//export const filter = (state = person => person, action) => {
//    switch(action.type){
//        case "SHOW_ATTENDING":
//            return person => person.attending;
//        case "SHOW_ALL":
//            return person => person;
//        case "SHOW_WITH_GUESTS":
//            return person => person.guests;
//        default:
//            return state;
//    }
//}
