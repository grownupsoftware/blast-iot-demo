import {Injectable} from '@angular/core';
import {Store, Action} from '@ngrx/store';
import {Observable} from 'rxjs/Observable';

import {IStore} from '../IStore';
import {Rule} from './rule';
import {ApplicationStore} from '../application-store';

const NAME: string = 'selectedRule';

// prefix all actions with NAME - dispatches are actioned on all stores, so need to differentiate
const SELECT: string = NAME + '_' + 'SELECT';
const RESET: string = NAME + '_' + 'RESET';


const EMPTY_RULE: Rule = {id: null, name: '', description: ''};

const REDUCER = (state: Rule = EMPTY_RULE, action: Action) => {
    console.log('rule-selected', action);
    switch (action.type) {
        case SELECT:
        case RESET:
            return Object.assign({}, action.payload);
        //return action.payload;
        default:
            console.log('rule-selected', 'default', 'state', state);
            return state;
    }
};

@Injectable()
export class RuleSelectedStoreService implements IStore {
    selectedRule: Observable<Rule>;

    constructor(private store: Store<ApplicationStore>) {
        this.selectedRule = store.select(NAME);
    }

    public static storeName(): string {
        return NAME;
    }

    public static reducer() {
        return REDUCER;
    }

    select(rule: Rule) {
        this.store.dispatch({type: SELECT, payload: rule});
    }

    reset() {
        this.store.dispatch({type: RESET, payload: EMPTY_RULE});
    }
}
