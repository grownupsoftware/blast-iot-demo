export interface Rule {
    id: number;
    name: string;
    description: string;
}