import {Injectable} from '@angular/core';
import {JsonPipe} from '@angular/common';
import {Store, Action} from '@ngrx/store';
import {Observable} from 'rxjs/Observable';
import {Effect, Actions, toPayload} from '@ngrx/effects';
import {IStore} from '../IStore';
import {Rule} from './rule';
import {ApplicationStore} from '../application-store';


import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/skip';
import 'rxjs/add/operator/takeUntil';
import 'rxjs/add/operator/toPromise';
import {empty} from 'rxjs/observable/empty';
import {of} from 'rxjs/observable/of';
import {defer} from 'rxjs/observable/defer';

import {RULES_DATA} from '../../test.data';

const NAME: string = 'rules';

// prefix all actions with NAME - dispatches are actioned on all stores, so need to differentiate
const ADD_ALL: string = NAME + '_' + 'ADD_ALL';
const INSERT: string = NAME + '_' + 'INSERT';
const UPDATE: string = NAME + '_' + 'UPDATE';
const REMOVE: string = NAME + '_' + 'REMOVE';


const REDUCER = (state: Rule[] = [], action: Action): any => {
    let pipe = new JsonPipe();
    console.log('rules-store', pipe.transform(action));
    switch (action.type) {
        case ADD_ALL:
            return action.payload;
        case INSERT:
            return [...state, action.payload];
        case UPDATE:
            return state.map(rule => {
                return rule.id === action.payload.id ? Object.assign({}, rule, action.payload) : rule;
            });
        case REMOVE:
            return state.filter(rule => rule.id !== action.payload.id);
        default:
            return state;
    }
};

@Injectable()
export class RulesStoreService implements IStore {
    idCounter: number = 100;
    rules: Observable<Array<Rule>>;

    constructor(private store: Store<ApplicationStore>, private actions: Actions) {
        this.rules = store.select(NAME);

        //this.rules = Observable.combineLatest(
        //			store.select(NAME),
        //			store.select('filter'),
        //			(people : any[], filter) => {
        //				return people.filter(filter);
        //			}
        //		)        
    }


    @Effect()
    search$: Observable<Action> = this.actions
        .ofType(INSERT)
        //.debounceTime(3000)
        .map(toPayload)
        .switchMap(rule => {
            console.log('inserted', rule);
            return empty();
        });


    public static storeName(): string {
        return NAME;
    }

    public static reducer() {
        return REDUCER;
    }

    attach() {
        this.store.dispatch({type: ADD_ALL, payload: RULES_DATA});
    }

    remove(rule: Rule) {
        this.store.dispatch({type: REMOVE, payload: rule});
    }

    save(rule: Rule) {
        rule.id === null ? this.create(rule) : this.update(rule);
        }

// -------------------- keep
//updateItem(item: Item    ) {
//  this.http.put(`${BASE_URL}${item.id}`, JSON.stringify(item), HEAD    ER)
//    .subscribe(action => this.store.dispatch({ type: 'UPDATE_ITEM', payload: item }    ));
    //}
//    update(rule: Rule): Observable<Action    > {
//        this.store.dispatch({type: UPDATE, payload: rule    });
//        return this.actions.ofType(UPDA    TE)
//            .map(toPayloa    d);
//        }
// ------------------

    private create(rule: Rule) {
        rule.id = this.idCounter++;
        this.store.dispatch({type: INSERT, payload: rule});
    }

    private update(rule: Rule) {
        this.store.dispatch({type: UPDATE, payload: rule});
    }

}