import {Rule} from './store/rule-store';
import {FORECAST_DATA} from './store/data/forecast.data';

export const RULES_DATA: Array<Rule> = [
    {id: 1, name: 'rule1', description: 'rule desc'}
];

export const HOME_DATA = {
    info: {
        'weather-forecast': {
            name: 'Weather Forecast',
            data: FORECAST_DATA
        }
    },
    things: [
        {
            id: 1,
            title: 'main Light',
            location: 'Lounge',
            category: 'Light',
            actions: [
                {
                    type: 'switch'
                }
            ],
            data: {

            }
        },
        {
            id: 2,
            title: 'wall Light',
            location: 'Lounge',
            category: 'Light',
            actions: [
                {
                    type: 'switch'
                },
                {
                    type: 'gauge'
                }
            ],
            data: {

            }
        },
        {
            id: 3,
            title: 'overhead Light',
            location: 'Kitchen',
            category: 'Light',
            actions: [
                {
                    type: 'switch'
                }
            ],
            data: {

            }
        },
        {
            id: 4,
            title: 'oven Light',
            location: 'Kitchen',
            category: 'Light',
            actions: [
                {
                    type: 'switch'
                },
                {
                    type: 'gauge'
                }
            ],
            data: {

            }
        },
        {
            id: 5,
            title: 'main Light',
            location: 'Main Bedroom',
            category: 'Light',
            actions: [
                {
                    type: 'switch'
                }
            ],
            data: {

            }
        },
        {
            id: 6,
            title: 'left bedside Light',
            location: 'Main Bedroom',
            category: 'Light',
            actions: [
                {
                    type: 'switch'
                },
                {
                    type: 'gauge'
                }
            ],
            data: {

            }
        },
        {
            id: 7,
            title: 'right bedside Light',
            location: 'Main Bedroom',
            category: 'Light',
            actions: [
                {
                    type: 'switch'
                },
                {
                    type: 'gauge'
                }
            ],
            data: {

            }
        }, {
            id: 8,
            title: 'main Light',
            location: 'Bedroom 2',
            category: 'Light',
            actions: [
                {
                    type: 'switch'
                }
            ],
            data: {

            }
        },
        {
            id: 9,
            title: 'bedside Light',
            location: 'Bedroom 2',
            category: 'Light',
            actions: [
                {
                    type: 'switch'
                },
                {
                    type: 'gauge'
                }
            ],
            data: {

            }
        }
    ]
};