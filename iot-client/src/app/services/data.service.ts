import {Injectable, OnInit, Inject} from '@angular/core';
import {StateService} from '@uirouter/angular';
import {Store} from '@ngrx/store';
import {Rule, RULE_STORE_NAME, SELECTED_RULE_STORE_NAME, RULE_FILTER_NAME, RULE_ACTION, RULE_FILTER} from './store/rule-store';
import {ApplicationStore} from './store/application-store';

import {RulesStoreService} from './store/rules/rules-store.service';

import {Observable} from 'rxjs/Observable';

import {HOME_DATA, RULES_DATA} from './test.data';

import {GraphBlastService} from 'blast-graph-angular2/blast';


@Injectable()
export class DataService implements OnInit {
    blastService: GraphBlastService;
    authenticated: boolean = false;
    loggedInUser: string;
    sessionToken: string;

    rules: Observable<Array<Rule>>;
    selectedRule: Observable<Rule>;


    constructor(private stateService: StateService,
        private rulesService: RulesStoreService,
        private store: Store<ApplicationStore>) {

        this.blastService = new GraphBlastService('ws://127.0.0.1:8081/blast');
        
        this.blastService.onOpen((msg: any) => {
            changeLight('green');
        });

        /**
         * Callback when server is disconnected
         */
        this.blastService.onClose((msg: any) => {
            changeLight('red');
        });

        function changeLight(color: any) {
            const trafficLightDiv = document.getElementById('trafficlight');
            trafficLightDiv.className = 'light ' + color;
        }

        this.blastService.onMessage((msg: any) => {
            console.log('recv: ', msg);
        });
        
        this.blastService.send({'cmd': 'discover'});

        this.rules = rulesService.rules;
        //this.rules = store.select(RULE_STORE_NAME);
        this.selectedRule = store.select(SELECTED_RULE_STORE_NAME);

        this.sessionToken = sessionStorage.getItem('blast-token');
        this.loggedInUser = sessionStorage.getItem('blast-user');
    }

    getBlastService(): GraphBlastService {
        return this.blastService;
    }

    attachRules() {
        this.store.dispatch({type: RULE_ACTION.ADD_RULES, payload: RULES_DATA});
        console.log('after attach', this.rules);
    }

    deleteRule(rule: Rule) {
        this.store.dispatch({type: RULE_ACTION.REMOVE_RULE, payload: rule});
    }

    saveRule(rule: Rule) {
        this.store.dispatch({type: RULE_ACTION.EDIT_RULE, payload: rule});
    }
    createRule(rule: Rule) {
        this.store.dispatch({type: RULE_ACTION.ADD_RULE, payload: this.addUUID(rule)});
    }
    updateRule(rule: Rule) {
        this.store.dispatch({type: RULE_ACTION.EDIT_RULE, payload: rule});
    }
    private addUUID(rule: Rule): Rule {
        return Object.assign({}, rule, {id: this.generateUUID()}); // Avoiding state mutation FTW!
    }
    private generateUUID(): string {
        return ('' + 1e7 + -1e3 + -4e3 + -8e3 + -1e11)
            .replace(/1|0/g, function () {
                return (0 | Math.random() * 16).toString(16);
            });
    };

    //loadItems() {
    //  // Retrieves the items collection, parses the JSON, creates an event with the JSON as a payload,
    //  // and dispatches that event
    //  this.http.get(BASE_URL)
    //    .map(res => res.json())
    //    .map(payload => ({ type: 'ADD_ITEMS', payload }))
    //    .subscribe(action => this.store.dispatch(action));
    //}

    //createItem(item: Item) {
    //  this.http.post(BASE_URL, JSON.stringify(item), HEADER)
    //    .map(res => res.json())
    //    .map(payload => ({ type: 'CREATE_ITEM', payload }))
    //    .subscribe(action => this.store.dispatch(action));
    //}
    //updateItem(item: Item) {
    //  this.http.put(`${BASE_URL}${item.id}`, JSON.stringify(item), HEADER)
    //    .subscribe(action => this.store.dispatch({ type: 'UPDATE_ITEM', payload: item }));
    //}
    //deleteItem(item: Item) {
    //  this.http.delete(`${BASE_URL}${item.id}`)
    //    .subscribe(action => this.store.dispatch({ type: 'DELETE_ITEM', payload: item }));
    //}

    ngOnInit() {
    }

    /**
     * Returns true if the user is currently authenticated, else false
    */
    isAuthenticated() {
        return this.sessionToken !== undefined && this.sessionToken !== null;
    }

    getUserName(): string {
        return this.loggedInUser;
    }

    /**
     * Login into the server (Remember: login is a command, therefore you can implement your own!)
     * @param username
     * @param password
     * @return {Promise}
     */
    login(username: string, password: string) {
        //        let promise: Promise<boolean> = new Promise<boolean>((resolve, reject) => {
        //            this.webSocketService.sendRequest('login', {username: username, password: password}).then((response) => {
        //                super.logger().info("sessionToken: ", response['sessionToken']);
        //                sessionStorage.setItem('blast-token', response['sessionToken']);
        //                sessionStorage.setItem('blast-user', username);
        //                this.authenticated = true;
        //                this.loggedInUser = username;
        //                attachMembersData(this.members);
        //                resolve(true);
        //            })
        //                .catch((error) => {
        //                    this.logger().error("Failed to login - error", error);
        //                    sessionStorage.removeItem('blast-token');
        //                    sessionStorage.removeItem('blast-user');
        //                    this.authenticated = false;
        //                    reject("Invalid username or password");
        //                });
        //
        //        });
        //        return promise;
    }



    logout() {
        //        this.logger().info("logging out - clearing session storage");
        //        this.authenticated = false;
        //        sessionStorage.removeItem('blast-token');
        //        sessionStorage.removeItem('blast-user');
        //        this.stateService.target('login');
    }

    attachData(collection: any) {
        if (Array.isArray(collection)) {
            Array.prototype.push.apply(collection, HOME_DATA);
        } else {
            this.mergeMap(collection, HOME_DATA);
        }
    }

    mergeMap(baseObject: any, changedObject: any) {
        for (const p in changedObject) {
            if (changedObject.hasOwnProperty(p)) {
                try {
                    // Property in destination object set; update its value.
                    if (changedObject[p].constructor === Object) {
                        baseObject[p] = this.mergeMap(baseObject[p], changedObject[p]);
                    } else {
                        baseObject[p] = changedObject[p];
                    }
                } catch (e) {
                    // Property in destination object not set; create it and set its value.
                    baseObject[p] = changedObject[p];
                }
            }
        }
    }

}
