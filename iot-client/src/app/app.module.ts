import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {UIRouterModule, UIView} from '@uirouter/angular';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FormsModule} from '@angular/forms';
import {StoreModule} from '@ngrx/store';

import {MaterialModule} from './material.module';
import {LayoutComponent, SettingsDialog} from './components/layout/layout.component';

import {APP_COMPONENTS, MENU_STATES, routerConfigFn} from './menu-states';
import {APPLICATION_STORES} from './services/store/application-store';

import {DataService} from './services/data.service';
import {WidgetModule} from './widget-controller/widget.module';

import {RULE_STORE_NAME, RulesReducer} from './services/store/rule-store';

import {ApplicationStoreModule} from './services/store/application-store.module';
import {SVGModule} from './components/svg/svg.module';

@NgModule({
    declarations: [
        ...APP_COMPONENTS
    ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        FormsModule,
        MaterialModule,
        UIRouterModule.forRoot({
            states: MENU_STATES,
            useHash: true,
            otherwise: {state: 'home'},
            config: routerConfigFn,
        }),
        ApplicationStoreModule,
        WidgetModule,
        SVGModule
    ],
    entryComponents: [SettingsDialog],
    providers: [
        {provide: DataService, useClass: DataService}
    ],
    bootstrap: [UIView]
})
export class AppModule {}
