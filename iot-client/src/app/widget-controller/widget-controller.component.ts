import {Inject, Component, ViewContainerRef, ViewChild, Input, ComponentRef, ChangeDetectorRef} from '@angular/core'
import {WidgetRegistra} from './widget-registra.service';

@Component({
    moduleId: module.id,
    selector: 'widget',
    template: `<div #target class="wrapper"></div>`,
    styles: ['.wrapper { display:inline-block;}']
})
export class WidgetController {
    @ViewChild('target', {read: ViewContainerRef}) target;
    @Input() tag;
    @Input() data;
    //    @Input() parentId;
    cmpRef: ComponentRef<any>;
    private isViewInitialized: boolean = false;

    constructor(
        private cdRef: ChangeDetectorRef,
        @Inject(WidgetRegistra) public widgetRegistra: WidgetRegistra
    ) {
    }

    updateComponent() {
        if (!this.isViewInitialized) {
            return;
        }
        if (this.cmpRef) {
            this.cmpRef.destroy();
        }

        console.log('widget', this.tag, this.data);

        if (this.tag === undefined) {
            return;
        }

        this.cmpRef = this.widgetRegistra.createComponent(this.tag, this.target);
        if (this.cmpRef !== undefined) {
            this.cmpRef.instance.setData(this.data);
        }

        //        this.cmpRef = this.target.createComponent(factory)
        //            this.cmpRef.instance.setData(this.data);
        //            this.cmpRef.instance.setParentId(this.parentId);
        // to access the created instance use
        // this.compRef.instance.someProperty = 'someValue';
        // this.compRef.instance.someOutput.subscribe(val => doSomething());
        this.cdRef.detectChanges();
    }

    ngOnChanges() {
        this.updateComponent();
    }

    ngAfterViewInit() {
        this.isViewInitialized = true;
        this.updateComponent();
    }

    ngOnDestroy() {
        if (this.cmpRef) {
            this.cmpRef.destroy();
        }
    }
}