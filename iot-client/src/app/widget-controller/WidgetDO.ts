import {Type} from '@angular/core'

export class WidgetDO<T> {
    tag: string;
    component: Type<T>;

    constructor(tag: string, component: Type<T>) {
        this.tag = tag;
        this.component = component;
    }
}