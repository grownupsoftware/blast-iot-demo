import {Type, Injectable, ViewContainerRef, ComponentFactoryResolver, ComponentRef, ReflectiveInjector} from '@angular/core';
import {Ng2StateDeclaration} from '@uirouter/angular';

import {WidgetDO} from './WidgetDO';

// -------------- adding a widget requires 3 steps
// step 1: import component
import {GaugeComponent} from '../widgets/gauge/gauge.component';
import {SwitchComponent} from '../widgets/switch/switch.component';
import {WeatherForecastComponent} from '../widgets/weather/forecast.component';
import {BulbComponent} from '../widgets/bulb/bulb.component';

import * as LIGHT from '../widgets/light/light.component';
import * as PLUG from '../widgets/plug/plug.component';
import * as KETTLE from '../widgets/kettle/kettle.component';
import * as CLOCK from '../widgets/clock/clock.component';

// step 2: add component to array
export const WIDGET_COMPONENTS = [
    ...LIGHT.COMPONENTS,
    ...PLUG.COMPONENTS,
    ...KETTLE.COMPONENTS,
    ...CLOCK.COMPONENTS,
    GaugeComponent,
    SwitchComponent,
    WeatherForecastComponent,
    BulbComponent
];

export const WIDGET_STATES: Ng2StateDeclaration[] = [
];

@Injectable()
export class WidgetRegistra {
    componentFactoryResolver: ComponentFactoryResolver;
    allWidgets: any = {};


    constructor(componentFactoryResolver: ComponentFactoryResolver) {
        this.componentFactoryResolver = componentFactoryResolver;

        this.registerTagAndComponents();

        const injector = ReflectiveInjector.resolveAndCreate(this.getRegisteredComponents());

    }
    add(tag: string, component: any) {
        this.allWidgets[tag] = new WidgetDO(tag, component);
    }
    registerTagAndComponents() {
        // step 3: register tag with component
        this.add("switch", SwitchComponent);
        this.add("gauge", GaugeComponent);
        this.add("weather-forecast", WeatherForecastComponent);
        this.add("light", LIGHT.LightComponent);
    }

    getRegisteredComponents() {
        let components: Type<any>[] = [];
        for (var k in this.allWidgets) {
            if (this.allWidgets.hasOwnProperty(k)) {
                components.push(this.allWidgets[k].component);
            }
        }
        return components;
    }

    createComponent(tag: string, container: ViewContainerRef): ComponentRef<any> {
        let device: WidgetDO<any> = this.allWidgets[tag];
        if (device === undefined) {
            console.log("failed to find device type for tag=", tag);
            return undefined;
        }
        const factory = this.componentFactoryResolver.resolveComponentFactory(device.component);
        if (factory === undefined) {
            console.log("faile dto resolve component factory");
            return undefined;
        }
        const componentRef: ComponentRef<any> = container.createComponent(factory);
        return componentRef;
    }
}
