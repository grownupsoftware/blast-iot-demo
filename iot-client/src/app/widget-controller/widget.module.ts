import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MaterialModule} from '../material.module';

import {WidgetController} from './widget-controller.component';
import {WIDGET_COMPONENTS, WidgetRegistra} from './widget-registra.service';
import {SVGModule} from '../components/svg/svg.module';

const OTHER_COMPONENTS: any[] = [
];

@NgModule({
    imports: [
        CommonModule,
        MaterialModule,
        SVGModule
    ],
    declarations: [
        ...WIDGET_COMPONENTS,
        ...OTHER_COMPONENTS,
        WidgetController
    ],
    providers: [
        {provide: WidgetRegistra, useClass: WidgetRegistra}
    ],
    entryComponents: [...WIDGET_COMPONENTS],
    exports: [
        ...WIDGET_COMPONENTS,
        WidgetController]

})
export class WidgetModule {};

//export {DeviceInterface} from './device-interface';
//export {AbstractMenuComponent} from './abstract-menu.component';
export {WidgetRegistra} from './widget-registra.service';
export {WIDGET_STATES} from './widget-registra.service';
