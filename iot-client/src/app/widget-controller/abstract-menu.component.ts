import {DataService} from '../services/data.service';

export class AbstractMenuComponent {
    parentId: number;
    private dataService: DataService;

    toggleControl: any = {};// {id:{open:boolean}} = {1:{open:true}};
    sectionToggle: boolean = true;

    constructor(dataService: DataService) {
        this.dataService = dataService;
    }

    getDataService(): DataService {
        return this.dataService;
    }

    setParentId(id: number) {
        this.parentId = id;
    }


    isOpen(id: number): boolean {
        const toggle = this.getToggle(id);
        return toggle.open;
    }

    getToggle(id: number): any {
        let toggle = this.toggleControl[id];
        if (toggle == undefined) {
            toggle = {open: true};
            this.toggleControl[id] = toggle;
        }
        return toggle;
    }

    toggle(id: number, event) {
        console.log('toggle', id);
        const toggle = this.getToggle(id);
        toggle.open = !toggle.open;
        // prevent parent clicks
        event.stopPropagation();
    }

    getKeys(data: any): string[] {
        let keys: string[] = [];
        for (var k in data) {
            if (data.hasOwnProperty(k)) {
                if (Array.isArray(data[k])) {
                    if (k !== 'memberIds') {
                        keys.push(k);
                    }
                }
            }
        }
        return keys;
    }

    getToggleIcon(id: number): string {
        return this.getToggle(id).open ? 'fa fa-caret-down' : 'fa fa-caret-right';
    }

    toggleSection(event) {
        this.sectionToggle = !this.sectionToggle;
        // prevent parent clicks
        event.stopPropagation();
    }

    getToggleSectionIcon(): string {
        return this.sectionToggle ? 'fa fa-caret-down' : 'fa fa-caret-right';
    }

    isSectionOpen(): boolean {
        return this.sectionToggle;
    }
}