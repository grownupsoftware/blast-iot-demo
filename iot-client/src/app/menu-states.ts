import {UIRouter, Category, Transition} from '@uirouter/angular';

import {AppComponent} from './app.component';
import {MainLayoutComponent} from './components/main-layout/main-layout.component';

//import {AboutComponent} from './about/about.component';
//import {HomeComponent} from './home/home.component';
//import {LoginComponent} from './login/login.component';
import {LeftMenuComponent} from './components/main-layout/left-menu/left-menu.component';
import {RightMenuComponent} from './components/main-layout/right-menu/right-menu.component';
import {TopMenuComponent} from './components/main-layout/top-menu/top-menu.component';
import {LogoComponent} from './components/logo/logo.component';

import {WIDGET_STATES} from './widget-controller/widget.module';
import * as RULE from './components/rules/rules.component';

// ------------ to add a new menu item = 4 steps

// Step 1: import component
import {RoomComponent} from './components/room/room.component';
import {CatgeoryComponent} from './components/category/category.component';
import {InfoComponent} from './components/info/info.component';
import {LayoutComponent, SettingsDialog} from './components/layout/layout.component';

// Step 2: add component to APP_COMPONENTS array
export const APP_COMPONENTS = [
    ...RULE.COMPONENTS,
    AppComponent,
    MainLayoutComponent,
    LeftMenuComponent,
    RightMenuComponent,
    TopMenuComponent,
    LogoComponent,
    RoomComponent,
    CatgeoryComponent,
    InfoComponent,
    LayoutComponent,
    SettingsDialog
];

// Step 3: add menu state
export const appState = {name: 'app', component: AppComponent, abstract: true};
export const homeState = {parent: 'app', name: 'home', url: '/home', component: MainLayoutComponent};
export const roomState = {name: 'home.room', url: '/room?:title', component: RoomComponent};
export const categoryState = {name: 'home.category', url: '/category?:title', component: CatgeoryComponent};
export const rulesState = {name: 'home.rules', url: '/rules', component: RULE.RulesComponent};
export const infoState = {name: 'home.info', url: '/info?:title&:key', component: InfoComponent};

// Step 4: add menu state to MENU_STATES array
export const MENU_STATES = [
    ...WIDGET_STATES,
    appState,
    homeState,
    roomState,
    categoryState,
    rulesState,
    infoState
];

export function routerConfigFn(router: UIRouter) {
    const transitionService = router.transitionService;
    //    requiresAuthHook(transitionService);
    router.trace.enable(Category.TRANSITION);
}
/**
 * A resolve function for 'login' state which figures out what state to return to, after a successful login.
 *
 * If the user was initially redirected to login state (due to the requiresAuth redirect), then return the toState/params
 * they were redirected from.  Otherwise, if they transitioned directly, return the fromState/params.  Otherwise
 * return the main "home" state.
 */
export function returnTo($transition$: Transition): any {
    if ($transition$.redirectedFrom() != null) {
        // The user was redirected to the login state (e.g., via the requiresAuth hook when trying to activate contacts)
        // Return to the original attempted target state (e.g., contacts)
        return $transition$.redirectedFrom().targetState();
    }

    const $state = $transition$.router.stateService;

    // The user was not redirected to the login state; they directly activated the login state somehow.
    // Return them to the state they came from.
    if ($transition$.from().name !== '') {
        return $state.target($transition$.from(), $transition$.params('from'));
    }

    // If the fromState's name is empty, then this was the initial transition. Just return them to the home state
    //FIXME
    return $state.target('home');
}
