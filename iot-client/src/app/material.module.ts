import {NgModule} from '@angular/core';

import {
    MdButtonModule,
    MdCheckboxModule,
    MdSlideToggleModule,
    MdMenuModule,
    MdIconModule,
    MdSidenavModule,
    MdToolbarModule,
    MdSelectModule,
    MdSliderModule,
    MdInputModule,
    MdListModule,
    MdGridListModule,
    MdButtonToggleModule,
    MdProgressSpinnerModule,
    MdDialogModule,
    MdProgressBarModule,
    MdSnackBarModule,
} from '@angular/material';

const COMPONENTS = [
    MdButtonModule,
    MdCheckboxModule,
    MdSlideToggleModule,
    MdMenuModule,
    MdIconModule,
    MdSidenavModule,
    MdToolbarModule,
    MdSelectModule,
    MdSliderModule,
    MdInputModule,
    MdListModule,
    MdGridListModule,
    MdButtonToggleModule,
    MdProgressSpinnerModule,
    MdDialogModule,
    MdProgressBarModule,
    MdSnackBarModule
];

@NgModule({
    imports: [...COMPONENTS],
    exports: [...COMPONENTS],
})
export class MaterialModule {}