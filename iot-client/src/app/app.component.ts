import {Component, OnInit} from '@angular/core';
import {DataService} from './services/data.service';


@Component({
    selector: 'blast-root',
    template: '<ui-view></ui-view>',
    styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
    constructor() {
    }
    ngOnInit() {
        // turn splash - off
        const splash = document.getElementById('splash');
        if (splash) {
            splash.setAttribute('style', 'display:none;');
        }
    }

}
