/*
 * Grownup Software Limited.
 */
package co.gusl.model;

import blast.doc.DocField;
import java.time.LocalDateTime;

/**
 *
 * @author grant
 */
public class TickerDO {

    @DocField(description = "Id for this ticker")
    private Long id;

    @DocField(description = "Title for this ticker")
    private String title;

    @DocField(description = "The time for this ticker")
    private LocalDateTime time;

    public TickerDO() {
    }

    public TickerDO(Long id, String title) {
        this.id = id;
        this.title = title;
        this.time = LocalDateTime.now();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public LocalDateTime getTime() {
        return time;
    }

    public void setTime(LocalDateTime time) {
        this.time = time;
    }

    @Override
    public String toString() {
        return "TickerDO{" + "id=" + id + ", title=" + title + ", time=" + time + '}';
    }

}
