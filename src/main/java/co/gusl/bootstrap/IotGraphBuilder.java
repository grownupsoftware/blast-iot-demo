package co.gusl.bootstrap;

import blast.exception.BlastException;
import blast.graph.core.BlastGraph;
import blast.graph.core.GraphCollection;
import blast.log.BlastLogger;
import co.gusl.model.TickerDO;

/**
 *
 * @author grant
 */
public class IotGraphBuilder {

    private final BlastLogger logger = BlastLogger.createLogger();

    private final BlastGraph theBlastGraph;

    public IotGraphBuilder(BlastGraph blastGraph) {
        this.theBlastGraph = blastGraph;
    }

    public void build() throws BlastException {
        GraphCollection<TickerDO, Long> tickers = theBlastGraph.createCollection()
                .name("tickers")
                .keyfield("id")
                .dataclass(TickerDO.class)
                .build();

        tickers.add(new TickerDO(1l, "IoT Time"));
    }
}
