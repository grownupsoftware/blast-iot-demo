/*
 * Grownup Software Limited.
 */
package co.gusl.bootstrap;

import static blast.BlastConstants.BLAST_ROUTE_PATH;
import blast.Blast;
import blast.exception.BlastException;
import blast.graph.core.BlastGraph;
import blast.graph.core.BlastGraphImpl;
import blast.graph.module.GraphModule;
import blast.server.BlastServer;
import co.gusl.iot.IoTModule;
import blast.server.config.BlastProperties;
import blast.vertx.engine.BlastRequestHandler;
import co.gusl.graph.IoTGraphCommandHandler;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpServerOptions;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.StaticHandler;

/**
 *
 * @author dhudson - May 11, 2017 - 8:35:54 AM
 */
public class IoTDemo {

    public static void main(String[] args) {
        try {

            Vertx vertx = Vertx.vertx();
            Router router = Router.router(vertx);

            // create a new Blast Graph
            BlastGraph blastGraph = new BlastGraphImpl();

            // create the graph module with the graph
            GraphModule graphModule = new GraphModule(blastGraph);

//            CommandModule commandModule = new CommandModule();
//            graphModule.addCommands(commandModule);
            BlastServer server = Blast.blast(new IoTModule(), graphModule);

            //FIXME - I thought configure would be called by Blast
            // aaah - so graphBuilder below is getting called before server does configure
            // so at the mo this is doubling the amount of data
            graphModule.configure(server);

            // command handler for Iot grap
            IoTGraphCommandHandler iotGraphCommandHandler = new IoTGraphCommandHandler(server, blastGraph);

            // build the graph
            IotGraphBuilder graphBuilder = new IotGraphBuilder(blastGraph);
            graphBuilder.build();

            BlastRequestHandler handler = new BlastRequestHandler(server);

            //Bind Blast
            router.route(BLAST_ROUTE_PATH).handler(routingContext -> {
                handler.handle(routingContext.request());
            });

            // Add static handler for web content
            StaticHandler staticHandler = StaticHandler.create("src/main/resources/webapp/demo");
            staticHandler.setIndexPage("index.html");
            staticHandler.setCachingEnabled(false);

            router.route("/*").handler(staticHandler);

            BlastProperties properties = server.getProperties();
            HttpServerOptions options = new HttpServerOptions();
            options.setAcceptBacklog(properties.getEndpoint().getBacklog());
            options.setPort(properties.getEndpoint().getPort());
            // Don't time out, need to do something with ping here
            options.setIdleTimeout(0);
            options.setReuseAddress(properties.isReuseAddress()).setReceiveBufferSize(properties.getInputBufferSize());
            options.setSendBufferSize(properties.getOutputBufferSize()).setSoLinger(properties.getSoLinger());
            options.setTcpNoDelay(properties.isNoDelay());

            server.startup();
            vertx.createHttpServer(options).requestHandler(router::accept).listen();

            server.startup();
        } catch (BlastException ex) {
            Blast.logger.error("Can't start IoT demo!", ex);
            System.err.print("Unable to start IoT demo");
            ex.printStackTrace();
        }

    }
}
