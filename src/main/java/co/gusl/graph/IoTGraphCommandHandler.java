/*
 * Grownup Software Limited.
 */
package co.gusl.graph;

import blast.eventbus.OnEvent;
import blast.exception.BlastException;
import blast.graph.core.BlastGraph;
import blast.graph.core.utils.GraphUtils;
import blast.log.BlastLogger;
import blast.server.BlastServer;
import co.gusl.iot.TickEvent;
import co.gusl.model.TickerDO;

/**
 *
 * @author grant
 */
public class IoTGraphCommandHandler {
    
    private final BlastLogger logger = BlastLogger.createLogger();
    
    private final BlastServer blastServer;
    private final BlastGraph blastGraph;

    // TickerDO and TickEvent s/be merged into one object
    private final TickerDO ticker = new TickerDO(1l, "IoT Time");
    
    public IoTGraphCommandHandler(BlastServer blastServer, BlastGraph blastGraph) {
        this.blastServer = blastServer;
        this.blastGraph = blastGraph;
        
        blastServer.registerEventListener(this);
    }
    
    @OnEvent
    public void handleTickEvent(TickEvent event) {
        try {
            ticker.setTime(event.getIotTime());
            blastGraph.update("tickers/id:1", GraphUtils.objectToMap(ticker));
            
        } catch (BlastException ex) {
            logger.error("Error updating tickevent in graph", ex);
        }
    }
}
