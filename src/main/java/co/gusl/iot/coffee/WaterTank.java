/*
 * Grownup Software Limited.
 */
package co.gusl.iot.coffee;

import co.gusl.iot.VolumeSensor;

/**
 *
 * @author dhudson - May 9, 2017 - 1:26:36 PM
 */
public class WaterTank extends VolumeSensor {

    // Ten cups
    public static final int FULL = 10;

    public WaterTank() {
        super(10);
    }

}
