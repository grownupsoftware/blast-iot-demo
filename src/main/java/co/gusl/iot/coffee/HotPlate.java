/*
 * Grownup Software Limited.
 */
package co.gusl.iot.coffee;

/**
 *
 * @author dhudson - May 9, 2017 - 1:24:35 PM
 */
public class HotPlate {

    public boolean isOn;
    public int temp;

    public HotPlate() {

    }

    public boolean isIsOn() {
        return isOn;
    }

    public void setIsOn(boolean isOn) {
        this.isOn = isOn;
    }

    public int getTemp() {
        return temp;
    }

    public void setTemp(int temp) {
        this.temp = temp;
    }

}
