/*
 * Grownup Software Limited.
 */
package co.gusl.iot.coffee;

import blast.eventbus.OnEvent;
import co.gusl.iot.ActionType;
import co.gusl.iot.Thing;
import co.gusl.iot.ThingAction;
import co.gusl.iot.TickEvent;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.UUID;

/**
 * Digital Twin of a IoT Coffee Pot.
 *
 * @author dhudson - May 9, 2017 - 1:23:52 PM
 */
public class GUCoffeePot extends Thing {

    private final HotPlate hotPlate;
    private boolean carafeSensor;
    private final WaterTank waterTank;
    private final BeanGrinder beanGrinder;
    private State state;
    private LocalTime brewed;

    public GUCoffeePot() {
        setID(UUID.randomUUID().toString());

        hotPlate = new HotPlate();
        waterTank = new WaterTank();
        beanGrinder = new BeanGrinder();
        // Lets assume that the carafe is present.
        carafeSensor = true;
        state = State.IDLE;

        addAction(new ThingAction("Start", ActionType.SWITCH));
        addAction(new ThingAction("SetAlarm", ActionType.TIME));
        addAction(new ThingAction("Reset", ActionType.DO));
    }

    @Override
    public void configureThing() {
        getBlastServer().registerEventListener(this);
    }

    @OnEvent
    public void handleTickEvent(TickEvent event) {
        // Only grind for 1 IoT Tick
        if (state == State.GRINDING) {
            if (beanGrinder.isEmpty()) {
                getBlastServer().postEvent(new OutOfBeansEvent());
            }
            state = State.BREWING;
            brewed = LocalDateTime.now().truncatedTo(ChronoUnit.MINUTES).plusMinutes(5).toLocalTime();
            return;
        }

        if (state == State.BREWING) {
            if (brewed == event.getIotTime().toLocalTime()) {
                //  We are done
                getBlastServer().postEvent(new CoffeeReadyEvent());
                state = State.IDLE;
            } else {
                waterTank.decreaseVolume(1);
                if (waterTank.isEmpty()) {
                    state = State.FAULT;
                    getBlastServer().postEvent(new OutOfWaterEvent());
                }
            }
        }
    }

    private void startToBrew() {
        if (state == State.IDLE) {
            if (beanGrinder.isEmpty() || waterTank.isEmpty()) {
                state = State.FAULT;
                return;
            }

            state = State.GRINDING;
            beanGrinder.decreaseVolume(5);
        }
    }

    @Override
    public void peformAction(String action) {
        if (action.equalsIgnoreCase("start")) {
            startToBrew();
        }

        if (action.equalsIgnoreCase("reset")) {
            waterTank.setVolume(WaterTank.FULL);
            beanGrinder.setVolume(BeanGrinder.FULL);
            state = State.IDLE;
        }
    }

    private enum State {

        IDLE,
        GRINDING,
        BREWING,
        FAULT
    }

    @Override
    public String toString() {
        return "GUCoffeePot{" + "hotPlate=" + hotPlate + ", carafeSensor=" + carafeSensor + ", waterTank=" + waterTank + ", beanGrinder=" + beanGrinder + ", state=" + state + ", brewed=" + brewed + '}';
    }

}
