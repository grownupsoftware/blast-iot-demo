/*
 * Grownup Software Limited.
 */
package co.gusl.iot.coffee;

import blast.exception.BlastException;
import blast.server.BlastServer;
import co.gusl.iot.Thing;
import co.gusl.iot.ThingModule;

/**
 *
 * @author dhudson - May 10, 2017 - 12:09:44 PM
 */
public class CoffeePotModule implements ThingModule {

    private final GUCoffeePot coffeePot;
    
    public CoffeePotModule() {
        coffeePot = new GUCoffeePot();
        coffeePot.setLocation("Kitchen");
        coffeePot.setTitle("Coffee Pot");
    }

    @Override
    public Thing getThing() {
       return coffeePot;
    }

    @Override
    public void configure(BlastServer bs) throws BlastException {
        
    }

    @Override
    public String getModuleID() {
       return coffeePot.getID();
    }

}
