/*
 * Grownup Software Limited.
 */
package co.gusl.iot.coffee;

import co.gusl.iot.VolumeSensor;

/**
 *
 * @author dhudson - May 9, 2017 - 1:42:57 PM
 */
public class BeanGrinder extends VolumeSensor {

    public static final int FULL = 20;

    public BeanGrinder() {
        super(FULL);
    }

}
