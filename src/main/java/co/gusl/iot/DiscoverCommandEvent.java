/*
 * Grownup Software Limited.
 */

package co.gusl.iot;

import blast.command.BlastCommandEvent;

/**
 *
 * @author dhudson - May 9, 2017 - 4:47:34 PM
 */
public class DiscoverCommandEvent extends BlastCommandEvent<DiscoverMessage> {

}
