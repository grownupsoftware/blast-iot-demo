/*
 * Grownup Software Limited.
 */
package co.gusl.iot;

/**
 *
 * @author dhudson - May 9, 2017 - 2:46:30 PM
 */
public class ThingAction {

    private String description;
    private ActionType type;

    public ThingAction() {

    }

    public ThingAction(String description, ActionType type) {
        this.description = description;
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ActionType getType() {
        return type;
    }

    public void setType(ActionType type) {
        this.type = type;
    }

}
