/*
 * Grownup Software Limited.
 */
package co.gusl.iot;

import java.time.LocalDateTime;

/**
 *
 * @author dhudson - May 9, 2017 - 4:43:58 PM
 */
public class TickEvent {

    private LocalDateTime iotTime;

    public TickEvent() {
    }

    public LocalDateTime getIotTime() {
        return iotTime;
    }

    public void setIotTime(LocalDateTime iotTime) {
        this.iotTime = iotTime;
    }
}
