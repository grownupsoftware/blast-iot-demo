/*
 * Grownup Software Limited.
 */
package co.gusl.iot.weather;

import com.fasterxml.jackson.annotation.JsonRootName;

/**
 *
 * @author dhudson - May 11, 2017 - 10:54:57 AM
 */
@JsonRootName("forecast")
public class HourlyForecast {

    private int humidityPercent;
    private String localDate;
    private int pressureMillibars;
    private Temp temperature;
    private String timeSlot;
    private String timeZone;
    private String visibility;
    private int weatherCode;
    private String weatherType;
    private WeatherSymbol weatherSymbol;
    private Wind wind;
    private String dayName;
    private String dayNameAbbreviation;
    
    public HourlyForecast() {
    }

    public int getHumidityPercent() {
        return humidityPercent;
    }

    public void setHumidityPercent(int humidityPercent) {
        this.humidityPercent = humidityPercent;
    }

    public String getLocalDate() {
        return localDate;
    }

    public void setLocalDate(String localDate) {
        this.localDate = localDate;
    }

    public int getPressureMillibars() {
        return pressureMillibars;
    }

    public void setPressureMillibars(int pressureMillibars) {
        this.pressureMillibars = pressureMillibars;
    }

    public Temp getTemperature() {
        return temperature;
    }

    public void setTemperature(Temp temperature) {
        this.temperature = temperature;
    }

    public String getTimeSlot() {
        return timeSlot;
    }

    public void setTimeSlot(String timeSlot) {
        this.timeSlot = timeSlot;
    }

    public String getTimeZone() {
        return timeZone;
    }

    public void setTimeZone(String timeZone) {
        this.timeZone = timeZone;
    }

    public String getVisibility() {
        return visibility;
    }

    public void setVisibility(String visibility) {
        this.visibility = visibility;
    }

    public int getWeatherCode() {
        return weatherCode;
    }

    public void setWeatherCode(int weatherCode) {
        this.weatherCode = weatherCode;
    }

    public String getWeatherType() {
        return weatherType;
    }

    public void setWeatherType(String weatherType) {
        this.weatherType = weatherType;
    }

    public WeatherSymbol getWeatherSymbol() {
        return weatherSymbol;
    }

    public void setWeatherSymbol(WeatherSymbol weatherSymbol) {
        this.weatherSymbol = weatherSymbol;
    }

    public Wind getWind() {
        return wind;
    }

    public void setWind(Wind wind) {
        this.wind = wind;
    }

    public String getDayName() {
        return dayName;
    }

    public void setDayName(String dayName) {
        this.dayName = dayName;
    }

    public String getDayNameAbbreviation() {
        return dayNameAbbreviation;
    }

    public void setDayNameAbbreviation(String dayNameAbbreviation) {
        this.dayNameAbbreviation = dayNameAbbreviation;
    }



}
