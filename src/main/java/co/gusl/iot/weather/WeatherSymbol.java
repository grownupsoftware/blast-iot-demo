/*
 * Grownup Software Limited.
 */
package co.gusl.iot.weather;

/**
 *
 * @author dhudson - May 10, 2017 - 8:42:45 AM
 */
public class WeatherSymbol {

    private String mobile;
    private String webMedium;
    private String webSmall;

    public WeatherSymbol() {
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getWebMedium() {
        return webMedium;
    }

    public void setWebMedium(String webMedium) {
        this.webMedium = webMedium;
    }

    public String getWebSmall() {
        return webSmall;
    }

    public void setWebSmall(String webSmall) {
        this.webSmall = webSmall;
    }

    @Override
    public String toString() {
        return "WeatherSymbol{" + "mobile=" + mobile + ", webMedium=" + webMedium + ", webSmall=" + webSmall + '}';
    }

}
