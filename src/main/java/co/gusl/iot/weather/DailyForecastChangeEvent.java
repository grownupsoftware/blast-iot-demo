/*
 * Grownup Software Limited.
 */
package co.gusl.iot.weather;

/**
 *
 * @author dhudson - May 12, 2017 - 10:53:53 AM
 */
public class DailyForecastChangeEvent {

    private DailyForecasts dailyForecasts;

    public DailyForecastChangeEvent() {
    }

    public DailyForecastChangeEvent(DailyForecasts dailyForecasts) {
        this.dailyForecasts = dailyForecasts;
    }

    public DailyForecasts getDailyForecasts() {
        return dailyForecasts;
    }

    public void setDailyForecasts(DailyForecasts dailyForecasts) {
        this.dailyForecasts = dailyForecasts;
    }

}
