/*
 * Grownup Software Limited.
 */
package co.gusl.iot.weather;

import blast.eventbus.OnEvent;
import blast.exception.BlastException;
import blast.log.BlastLogger;
import co.gusl.iot.Thing;
import co.gusl.iot.TickEvent;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.fasterxml.jackson.module.afterburner.AfterburnerModule;
import de.danielbechler.diff.ObjectDifferBuilder;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.time.LocalTime;
import java.time.OffsetDateTime;
import java.util.UUID;

/**
 * https://github.com/java-json-tools/json-patch
 * https://github.com/SQiShER/java-object-diff
 *
 * @author dhudson - May 10, 2017 - 8:27:42 AM
 */
public class GUWeatherStation extends Thing {

    // Weather for Maidenhead for the next three hours
    @JsonIgnore
    private static final String BBC_URL_HOURS = "http://open.live.bbc.co.uk/weather/feeds/en/2643186/3hourlyforecast.json";
    @JsonIgnore
    private static final String BBC_URL_DAYS = "http://open.live.bbc.co.uk/weather/feeds/en/2643186/3dayforecast.json";
    @JsonIgnore
    private final BlastLogger logger = BlastLogger.createLogger();
    @JsonIgnore
    private final ObjectMapper objectMapper;
    
    private HourlyForecasts hourlyForecasts;
    private DailyForecasts dailyForecasts;
    
    private LocalTime sunrise;
    private LocalTime sunset;
    
    private long ticks;
    
    public GUWeatherStation() {
        setID(UUID.randomUUID().toString());
        objectMapper = new ObjectMapper();
        // Java Time
        objectMapper.registerModule(new JavaTimeModule());
        objectMapper.registerModule(new AfterburnerModule());
    }
    
    @Override
    public void configureThing() {
        getBlastServer().registerEventListener(this);
    }
    
    public void weatherDeltas() {
        logger.info("weatherDeltas");
        try {
            DailyForecasts dForecasts = getDailyForecasts();
            if (ObjectDifferBuilder.buildDefault().compare(dForecasts, dailyForecasts).hasChanges()) {
                
                dailyForecasts = dForecasts;
                
                sunrise = OffsetDateTime.parse(dailyForecasts.getForecasts().get(0).getDay().getSunrise()).toLocalTime();
                sunset = OffsetDateTime.parse(dailyForecasts.getForecasts().get(0).getNight().getSunset()).toLocalTime();
                
                logger.info("DailyForecastChangeEvent");
                getBlastServer().postEvent(new DailyForecastChangeEvent(dailyForecasts));
            }
            
            HourlyForecasts hForecasts = getHourlyForecasts();
            if (ObjectDifferBuilder.buildDefault().compare(hForecasts, hourlyForecasts).hasChanges()) {
                
                hourlyForecasts = hForecasts;
                
                logger.info("HourlyForecastChangeEvent");
                getBlastServer().postEvent(new HourlyForecastChangeEvent(hourlyForecasts));
            }
        } catch (BlastException ex) {
            logger.warn("Unable to process weather deltas", ex);
        }
    }
    
    private String getWeatherData(String location) throws BlastException {
        
        HttpURLConnection connection = null;
        
        try {
            //Create connection
            URL url = new URL(location);
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.setRequestProperty("Content-Type", "application/json");
            
            connection.setUseCaches(false);
            connection.setDoOutput(true);
            
            int responseCode = connection.getResponseCode();
            if (responseCode == 200) {
                logger.info("Response Code {}", responseCode);
                
                InputStream is = connection.getInputStream();
                try (BufferedReader rd = new BufferedReader(new InputStreamReader(is))) {
                    StringBuilder response = new StringBuilder(500);
                    String line;
                    while ((line = rd.readLine()) != null) {
                        response.append(line);
                    }
                    
                    return response.toString();
                }
            }
            
        } catch (IOException ex) {
            throw new BlastException("Unable to obtain weather", ex);
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }
        return null;
    }
    
    private HourlyForecasts getHourlyForecasts() throws BlastException {
        
        String data = getWeatherData(BBC_URL_HOURS);
        try {
            JsonNode node = objectMapper.readTree(data);
            return objectMapper.treeToValue(node.get("forecastContent"), HourlyForecasts.class);
        } catch (IOException ex) {
            throw new BlastException("Unable to parse JSON", ex);
        }
    }
    
    private DailyForecasts getDailyForecasts() throws BlastException {
        String data = getWeatherData(BBC_URL_DAYS);
        try {
            JsonNode node = objectMapper.readTree(data);
            return objectMapper.treeToValue(node.get("forecastContent"), DailyForecasts.class);
        } catch (IOException ex) {
            throw new BlastException("Unable to parse JSON", ex);
        }
    }
    
    @OnEvent
    public void handleTick(TickEvent event) {
        // Lets see if its sun rise or sun set
        LocalTime iotTime = event.getIotTime().toLocalTime();
        
        ticks++;

        // Every 10 minutes
        if (ticks % 600 == 0) {
            weatherDeltas();
        }
        
        if (iotTime.equals(sunrise)) {
            logger.info("Sunrise Event");
            getBlastServer().postEvent(new SunriseEvent());
        }
        
        if (iotTime.equals((sunset))) {
            logger.info("sunset event");
            getBlastServer().postEvent(new SunsetEvent());
        }
    }

    @Override
    public void peformAction(String action) {
       // No actions to peform
    }

    @Override
    public String toString() {
        return "GUWeatherStation{ hourlyForecasts=" + hourlyForecasts + ", dailyForecasts=" + dailyForecasts + ", sunrise=" + sunrise + ", sunset=" + sunset + ", ticks=" + ticks + '}';
    }
    
    
}
