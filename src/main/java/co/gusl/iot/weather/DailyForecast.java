/*
 * Grownup Software Limited.
 */

package co.gusl.iot.weather;

import com.fasterxml.jackson.annotation.JsonRootName;

/**
 *
 * @author dhudson - May 10, 2017 - 8:29:48 AM
 */
@JsonRootName("forecast")
public class DailyForecast {

    private String date;
    private String dayName;
    private String dayNameAbbreviation;
    private Day day;
    private Night night;
    
    public DailyForecast() {}

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDayName() {
        return dayName;
    }

    public void setDayName(String dayName) {
        this.dayName = dayName;
    }

    public String getDayNameAbbreviation() {
        return dayNameAbbreviation;
    }

    public void setDayNameAbbreviation(String dayNameAbbreviation) {
        this.dayNameAbbreviation = dayNameAbbreviation;
    }

    public Day getDay() {
        return day;
    }

    public void setDay(Day day) {
        this.day = day;
    }

    public Night getNight() {
        return night;
    }

    public void setNight(Night night) {
        this.night = night;
    }

    @Override
    public String toString() {
        return "Forecast{" + "date=" + date + ", dayName=" + dayName + ", dayNameAbbreviation=" + dayNameAbbreviation + ", day=" + day + ", night=" + night + '}';
    }
    
}
