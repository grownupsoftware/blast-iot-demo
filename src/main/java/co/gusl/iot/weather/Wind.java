/*
 * Grownup Software Limited.
 */
package co.gusl.iot.weather;

/**
 *
 * @author dhudson - May 10, 2017 - 8:41:11 AM
 */
public class Wind {

    private String direction;
    private String directionDesc;
    private Speed windspeed;
    private String iconGrey;
    private String iconWhite;

    public Wind() {
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public String getDirectionDesc() {
        return directionDesc;
    }

    public void setDirectionDesc(String directionDesc) {
        this.directionDesc = directionDesc;
    }

    public Speed getWindspeed() {
        return windspeed;
    }

    public void setWindspeed(Speed windspeed) {
        this.windspeed = windspeed;
    }

    public String getIconGrey() {
        return iconGrey;
    }

    public void setIconGrey(String iconGrey) {
        this.iconGrey = iconGrey;
    }

    public String getIconWhite() {
        return iconWhite;
    }

    public void setIconWhite(String iconWhite) {
        this.iconWhite = iconWhite;
    }

    @Override
    public String toString() {
        return "Wind{" + "direction=" + direction + ", directionDesc=" + directionDesc + ", windspeed=" + windspeed + ", iconGrey=" + iconGrey + ", iconWhite=" + iconWhite + '}';
    }

}
