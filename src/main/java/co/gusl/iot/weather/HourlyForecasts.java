/*
 * Grownup Software Limited.
 */
package co.gusl.iot.weather;

import com.fasterxml.jackson.annotation.JsonRootName;
import java.util.List;

/**
 *
 * @author dhudson - May 11, 2017 - 11:34:32 AM
 */
@JsonRootName("ForecastContent")
public class HourlyForecasts {

    private List<HourlyForecast> forecasts;
    private Location location;

    public HourlyForecasts() {
    }

    public List<HourlyForecast> getForecasts() {
        return forecasts;
    }

    public void setForecasts(List<HourlyForecast> forecasts) {
        this.forecasts = forecasts;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    @Override
    public String toString() {
        return "HourlyForecasts{" + "forecasts=" + forecasts + ", location=" + location + '}';
    }

}
