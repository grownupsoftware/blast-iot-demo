/*
 * Grownup Software Limited.
 */
package co.gusl.iot.weather;

import java.util.List;

/**
 *
 * @author dhudson - May 10, 2017 - 8:29:36 AM
 */
public class ForecastContent {

    private List<DailyForecast> forecasts;
    private Location location;

    public ForecastContent() {
    }

    public List<DailyForecast> getForecasts() {
        return forecasts;
    }

    public void setForecasts(List<DailyForecast> forecasts) {
        this.forecasts = forecasts;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    @Override
    public String toString() {
        return "ForecastContent{" + "forecasts=" + forecasts + ", location=" + location + '}';
    }

}
