/*
 * Grownup Software Limited.
 */
package co.gusl.iot.weather;

/**
 *
 * @author dhudson - May 10, 2017 - 8:33:25 AM
 */
public class Day {

    private int humidityPercent;
    private Temp maxTemperature;
    private int pressureMillibars;
    private String sunrise;
    private String visibility;
    private String weatherType;
    private int weatherCode;
    private Wind wind;
    private WeatherSymbol weatherSymbol;

    public Day() {
    }

    public int getHumidityPercent() {
        return humidityPercent;
    }

    public void setHumidityPercent(int humidityPercent) {
        this.humidityPercent = humidityPercent;
    }

    public int getPressureMillibars() {
        return pressureMillibars;
    }

    public void setPressureMillibars(int pressureMillibars) {
        this.pressureMillibars = pressureMillibars;
    }

    public String getSunrise() {
        return sunrise;
    }

    public void setSunrise(String sunrise) {
        this.sunrise = sunrise;
    }

    public String getVisibility() {
        return visibility;
    }

    public void setVisibility(String visibility) {
        this.visibility = visibility;
    }

    public String getWeatherType() {
        return weatherType;
    }

    public void setWeatherType(String weatherType) {
        this.weatherType = weatherType;
    }

    public Wind getWind() {
        return wind;
    }

    public void setWind(Wind wind) {
        this.wind = wind;
    }

    public WeatherSymbol getWeatherSymbol() {
        return weatherSymbol;
    }

    public void setWeatherSymbol(WeatherSymbol weatherSymbol) {
        this.weatherSymbol = weatherSymbol;
    }

    public int getWeatherCode() {
        return weatherCode;
    }

    public void setWeatherCode(int weatherCode) {
        this.weatherCode = weatherCode;
    }

    public Temp getMaxTemperature() {
        return maxTemperature;
    }

    public void setMaxTemperature(Temp maxTemperature) {
        this.maxTemperature = maxTemperature;
    }

    @Override
    public String toString() {
        return "Day{" + "humidityPercent=" + humidityPercent + ", maxTemperature=" + maxTemperature + ", pressureMillibars=" + pressureMillibars + ", sunrise=" + sunrise + ", visibility=" + visibility + ", weatherType=" + weatherType + ", weatherCode=" + weatherCode + ", wind=" + wind + ", weatherSymbol=" + weatherSymbol + '}';
    }
    
}
