/*
 * Grownup Software Limited.
 */
package co.gusl.iot.weather;

/**
 *
 * @author dhudson - May 10, 2017 - 8:33:34 AM
 */
public class Night {

    private Temp minTemperature;
    private String sunset;
    private WeatherSymbol weatherSymbol;
    private String weatherType;
    private int weatherCode;
    
    public Night() {
    }

    public Temp getMinTemperature() {
        return minTemperature;
    }

    public void setMinTemperature(Temp minTemperature) {
        this.minTemperature = minTemperature;
    }

    public String getSunset() {
        return sunset;
    }

    public void setSunset(String sunset) {
        this.sunset = sunset;
    }

    public WeatherSymbol getWeatherSymbol() {
        return weatherSymbol;
    }

    public void setWeatherSymbol(WeatherSymbol weatherSymbol) {
        this.weatherSymbol = weatherSymbol;
    }

    public String getWeatherType() {
        return weatherType;
    }

    public void setWeatherType(String weatherType) {
        this.weatherType = weatherType;
    }

    public int getWeatherCode() {
        return weatherCode;
    }

    public void setWeatherCode(int weatherCode) {
        this.weatherCode = weatherCode;
    }

    @Override
    public String toString() {
        return "Night{" + "minTemperature=" + minTemperature + ", sunset=" + sunset + ", weatherSymbol=" + weatherSymbol + ", weatherType=" + weatherType + ", weatherCode=" + weatherCode + '}';
    }
    
}
