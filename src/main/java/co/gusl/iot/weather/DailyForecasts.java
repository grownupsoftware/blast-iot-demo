/*
 * Grownup Software Limited.
 */
package co.gusl.iot.weather;

import java.util.List;

/**
 *
 * @author dhudson - May 11, 2017 - 11:34:21 AM
 */
public class DailyForecasts {

    private List<DailyForecast> forecasts;
    private Location location;

    public DailyForecasts() {
    }

    public List<DailyForecast> getForecasts() {
        return forecasts;
    }

    public void setForecasts(List<DailyForecast> forecasts) {
        this.forecasts = forecasts;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    @Override
    public String toString() {
        return "DailyForecasts{" + "forecasts=" + forecasts + ", location=" + location + '}';
    }

}
