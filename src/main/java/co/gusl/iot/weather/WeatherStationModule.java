/*
 * Grownup Software Limited.
 */
package co.gusl.iot.weather;

import blast.exception.BlastException;
import blast.server.BlastServer;
import co.gusl.iot.Thing;
import co.gusl.iot.ThingModule;

/**
 *
 * @author dhudson - May 10, 2017 - 12:13:38 PM
 */
public class WeatherStationModule implements ThingModule {

    private final GUWeatherStation weatherStation;

    public WeatherStationModule() {
        weatherStation = new GUWeatherStation();
        weatherStation.setLocation("Garden");
        weatherStation.setTitle("Weather Station");
    }

    @Override
    public Thing getThing() {
        return weatherStation;
    }

    @Override
    public void configure(BlastServer bs) throws BlastException {
        weatherStation.configure(bs);
        weatherStation.weatherDeltas();
    }

    @Override
    public String getModuleID() {
        return weatherStation.getID();
    }

}
