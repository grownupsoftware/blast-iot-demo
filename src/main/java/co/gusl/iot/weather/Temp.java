/*
 * Grownup Software Limited.
 */
package co.gusl.iot.weather;

/**
 *
 * @author dhudson - May 10, 2017 - 8:37:27 AM
 */
public class Temp {

    private int centigrade;
    private int fahrenheit;

    public Temp() {
    }

    public int getCentigrade() {
        return centigrade;
    }

    public void setCentigrade(int centigrade) {
        this.centigrade = centigrade;
    }

    public int getFahrenheit() {
        return fahrenheit;
    }

    public void setFahrenheit(int fahrenheit) {
        this.fahrenheit = fahrenheit;
    }

    @Override
    public String toString() {
        return "Temp{" + "centigrade=" + centigrade + ", fahrenheit=" + fahrenheit + '}';
    }

}
