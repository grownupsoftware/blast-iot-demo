/*
 * Grownup Software Limited.
 */
package co.gusl.iot.weather;

/**
 *
 * @author dhudson - May 10, 2017 - 8:36:32 AM
 */
public class Speed {

    private int kph;
    private int mph;

    public Speed() {
    }

    public int getKph() {
        return kph;
    }

    public void setKph(int kph) {
        this.kph = kph;
    }

    public int getMph() {
        return mph;
    }

    public void setMph(int mph) {
        this.mph = mph;
    }

    @Override
    public String toString() {
        return "Speed{" + "kph=" + kph + ", mph=" + mph + '}';
    }

}
