/*
 * Grownup Software Limited.
 */
package co.gusl.iot.weather;

/**
 *
 * @author dhudson - May 12, 2017 - 11:01:47 AM
 */
public class HourlyForecastChangeEvent {

    private HourlyForecasts hourlyForecasts;

    public HourlyForecastChangeEvent() {
    }

    public HourlyForecastChangeEvent(HourlyForecasts hourlyForecasts) {
        this.hourlyForecasts = hourlyForecasts;
    }

    public HourlyForecasts getHourlyForecasts() {
        return hourlyForecasts;
    }

    public void setHourlyForecasts(HourlyForecasts hourlyForecasts) {
        this.hourlyForecasts = hourlyForecasts;
    }

}
