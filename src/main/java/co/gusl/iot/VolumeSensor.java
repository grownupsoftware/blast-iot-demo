/*
 * Grownup Software Limited.
 */
package co.gusl.iot;

/**
 *
 * @author dhudson - May 9, 2017 - 1:47:19 PM
 */
public class VolumeSensor {

    private final int max;
    private int volume;

    public VolumeSensor(int max) {
        this.max = max;
    }

    public boolean isEmpty() {
        return volume == 0;
    }

    public boolean isFull() {
        return volume == max;
    }

    public int getVolume() {
        return volume;
    }

    public void setVolume(int volume) {
        this.volume = volume;
        if (this.volume > max) {
            this.volume = max;
        } else if (this.volume < 0) {
            this.volume = 0;
        }
    }

    public void decreaseVolume(int amount) {
        volume -= amount;
        if (volume < 0) {
            volume = 0;
        }
    }
}
