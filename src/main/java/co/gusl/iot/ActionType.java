/*
 * Grownup Software Limited.
 */
package co.gusl.iot;

/**
 *
 * @author dhudson - May 9, 2017 - 4:17:12 PM
 */
public enum ActionType {

    DO,
    SWITCH,
    RANGE,
    TIME
}
