/*
 * Grownup Software Limited.
 */
package co.gusl.iot;

import blast.command.CommandMessage;

/**
 *
 * @author dhudson - May 15, 2017 - 2:21:02 PM
 */
public class IoTActionMessage extends CommandMessage {

    private String id;

    private String action;

    public IoTActionMessage() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

}
