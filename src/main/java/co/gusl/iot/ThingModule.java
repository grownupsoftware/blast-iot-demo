/*
 * Grownup Software Limited.
 */

package co.gusl.iot;

import blast.module.BlastModule;

/**
 *
 * @author dhudson - May 10, 2017 - 11:44:15 AM
 */
public interface ThingModule extends BlastModule {

    public Thing getThing();
    
}
