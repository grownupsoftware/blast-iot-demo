/*
 * Grownup Software Limited.
 */
package co.gusl.iot;

import blast.server.BlastServer;
import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.ArrayList;
import java.util.List;

/**
 * Base Class for a 'Thing'
 *
 * @author dhudson - May 9, 2017 - 2:25:02 PM
 */
public abstract class Thing {

    private String ID;
    private String title;
    private String location;
    private final List<ThingAction> actions;
    private BlastServer server;

    public Thing() {
        actions = new ArrayList<>(2);
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public final void addAction(ThingAction action) {
        actions.add(action);
    }

    public List<ThingAction> getActions() {
        return actions;
    }

    public void configure(BlastServer server) {
        this.server = server;
        configureThing();
    }

    @JsonIgnore
    public BlastServer getBlastServer() {
        return server;
    }

    public abstract void configureThing();

    public abstract void peformAction(String action);
}
