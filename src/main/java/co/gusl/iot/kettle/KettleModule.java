/*
 * Grownup Software Limited.
 */
package co.gusl.iot.kettle;

import blast.exception.BlastException;
import blast.server.BlastServer;
import co.gusl.iot.Thing;
import co.gusl.iot.ThingModule;

/**
 * Kettle Nano Service.
 *
 * @author dhudson - May 10, 2017 - 11:43:08 AM
 */
public class KettleModule implements ThingModule {

    private final GUKettle kettle;

    public KettleModule() {
        kettle = new GUKettle();
        kettle.setLocation("Kitchen");
        kettle.setTitle("Kettle");
    }

    @Override
    public void configure(BlastServer bs) throws BlastException {
        kettle.configure(bs);
    }

    @Override
    public String getModuleID() {
        return kettle.getID();
    }

    @Override
    public Thing getThing() {
        return kettle;
    }
}
