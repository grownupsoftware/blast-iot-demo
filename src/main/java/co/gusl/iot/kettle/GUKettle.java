/*
 * Grownup Software Limited.
 */
package co.gusl.iot.kettle;

import blast.eventbus.OnEvent;
import co.gusl.iot.ActionType;
import co.gusl.iot.Thing;
import co.gusl.iot.ThingAction;
import co.gusl.iot.TickEvent;
import com.fasterxml.jackson.annotation.JsonIgnore;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.UUID;

/**
 * Digital Twin of an IoT Kettle.
 *
 * @author dhudson - May 9, 2017 - 2:19:09 PM
 */
public class GUKettle extends Thing {

    private State state;

    private LocalTime boiled;

    public GUKettle() {
        super();
        boiled = LocalDateTime.now().toLocalTime();

        setID(UUID.randomUUID().toString());
        state = State.IDLE;
        addAction(new ThingAction("Start", ActionType.SWITCH));
        addAction(new ThingAction("Stop", ActionType.SWITCH));

    }

    public State getState() {
        return state;
    }

    @JsonIgnore
    public LocalTime getBoiled() {
        return boiled;
    }

    @Override
    public void configureThing() {
        getBlastServer().registerEventListener(this);
    }

    @OnEvent
    public void handleTickEvent(TickEvent event) {
        if (state == State.BOILING) {
            if (event.getIotTime().toLocalTime().equals(boiled)) {
                state = State.IDLE;
                getBlastServer().postEvent(new KettleBoiledEvent());
            }
        }
    }

    private void startToBoil() {
        if (state == State.BOILING) {
            return;
        }

        state = State.BOILING;
        boiled = LocalDateTime.now().truncatedTo(ChronoUnit.MINUTES).plusMinutes(2).toLocalTime();
    }

    @Override
    public void peformAction(String action) {
        if (action.equalsIgnoreCase("start")) {
            startToBoil();
        } else if (action.equalsIgnoreCase("stop")) {
            state = State.IDLE;
        }
    }

    private enum State {

        IDLE,
        BOILING
    }

    @Override
    public String toString() {
        return "GUKettle{" + "state=" + state + ", boiled=" + boiled + '}';
    }

}
