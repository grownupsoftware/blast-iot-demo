/*
 * Grownup Software Limited.
 */
package co.gusl.iot;

import blast.Controllable;
import blast.client.BlastServerClient;
import blast.exception.BlastException;
import blast.module.BlastModule;
import blast.server.BlastServer;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import blast.command.CommandModule;
import blast.eventbus.OnEvent;
import blast.json.ObjectMapperFactory;
import blast.log.BlastLogger;
import blast.utils.BlastUtils;
import co.gusl.iot.coffee.CoffeePotModule;
import co.gusl.iot.kettle.KettleModule;
import com.fasterxml.jackson.core.JsonProcessingException;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author dhudson - May 9, 2017 - 11:17:55 AM
 */
public class IoTModule implements BlastModule, Controllable {

    private final BlastLogger logger = BlastLogger.createLogger();

    public static final String IOT_MODULE_ID = "co.gusl.iot";
    private final Map<String, Thing> things;
    private final ScheduledExecutorService service;
    private BlastServer server;
    private ScheduledFuture<?> tickFuture;

    // Iot Clock. We want each second to be a minute, or its a very slow demo.
    private LocalDateTime iotTime;

    public IoTModule() {
        things = new HashMap<>();
        service = Executors.newScheduledThreadPool(1);
    }

    @Override
    public void configure(BlastServer bs) throws BlastException {
        // Lets build the house of things
        server = bs;

        // Requires the Command Module
        CommandModule commandModule = (CommandModule) server.requireModule(CommandModule.COMMAND_DECODER_MODULE_ID, CommandModule.class);

        // Command handler
        commandModule.registerCommand("discover", DiscoverCommandEvent.class, DiscoverMessage.class);
        commandModule.registerCommand("action", IoTActionEvent.class, IoTActionMessage.class);

        // Lets add the things in the house
        buildHouseOfThings();

        bs.registerEventListener(this);
    }

    private void buildHouseOfThings() throws BlastException {
        registerThingModule(new CoffeePotModule());
        registerThingModule(new KettleModule());
        //registerThingModule(new WeatherStationModule());
    }

    private void registerThingModule(ThingModule module) throws BlastException {
        server.registerModule(module);
        things.put(module.getModuleID(), module.getThing());
    }

    @Override
    public String getModuleID() {
        return IOT_MODULE_ID;
    }

    @Override
    public void startup() throws BlastException {

        // Start the clock
        iotTime = LocalDateTime.now().truncatedTo(ChronoUnit.MINUTES);

        service.scheduleAtFixedRate(() -> {
            server.postEvent(new TickEvent());
        }, 1, 1, TimeUnit.SECONDS);
    }

    @Override
    public void shutdown() {
        tickFuture.cancel(true);
        service.shutdownNow();
    }

    @OnEvent
    public void discoveryHandler(DiscoverCommandEvent event) {
        BlastServerClient client = event.getClient();
        try {
            logger.info("sending: {}", things.values());
            BlastUtils.safeStream(things.values()).forEach(entry -> {
                try {
                    logger.info("entry: {}", entry.getTitle());
                    logger.info("entry: {}",ObjectMapperFactory.createObjectMapper().writeValueAsString(entry));
                    client.queueMessage(entry);
                } catch (JsonProcessingException | BlastException ex) {
                    logger.warn("Unable to send message to {}", client,ex);
                }
            });
            client.queueMessage(things.values());
        } catch (BlastException ex) {
            logger.warn("Unable to send message to {}", client);
//            logger.warn("Unable to send message to {}", client, ex);
        }
    }

    @OnEvent
    public void iotActionHandler(IoTActionEvent event
    ) {
        String id = event.getCommandData().getId();
        String action = event.getCommandData().getAction();

        if (action == null) {
            logger.warn("No action to perform");
            return;
        }

        Thing thing = things.get(id);
        if (thing == null) {
            logger.warn("Don't have a 'Thing' with an ID of ", id);
            return;
        }

        if (action.equalsIgnoreCase("status")) {
            BlastServerClient client = event.getClient();
            try {
                client.queueMessage(thing);
            } catch (BlastException ex) {
                logger.warn("Unable to send message to {}", client, ex);
            }

            return;
        }

        thing.peformAction(action);
    }

    @OnEvent(order = 10)
    public void tickHandler(TickEvent event
    ) {
        iotTime = iotTime.plusMinutes(1);
        event.setIotTime(iotTime);

        //logger.info("Tick {}", iotTime);
    }
}
