/*
 * Grownup Software Limited.
 */
package co.gusl.iot.kettle;

import blast.Blast;
import blast.json.ObjectMapperFactory;
import blast.log.BlastLogger;
import blast.server.BlastServer;
import blast.utils.BlastUtils;
import co.gusl.iot.Thing;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.HashMap;
import java.util.Map;
import org.junit.Test;

/**
 *
 * @author dhudson - May 17, 2017 - 2:08:45 PM
 */
public class KettleTester {

    BlastLogger logger = BlastLogger.createLogger();

    @Test
    public void kettleTest() {

        BlastServer server = Blast.blast();
        
        ObjectMapper mapper = ObjectMapperFactory.createObjectMapper();
        GUKettle kettle = new GUKettle();
        kettle.configure(server);
        
        Map<String, Thing> things = new HashMap<>();
        things.put(kettle.getID(), kettle);

        for (Thing thing : things.values()) {
            try {
                logger.info(" Its {}", mapper.writeValueAsString(thing));
                BlastUtils.sleep(1000);
            } catch (JsonProcessingException ex) {
                assert (false);
            }
        }

    }

}
